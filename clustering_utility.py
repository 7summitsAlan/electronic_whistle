# -*- coding: utf-8 -*-
"""
K-Means on New Zealand data
Multiple feature comparison: MFCC, HOG and spectrogram

mini-batch

Created on Thu Jun 22 11:08:16 2017

@author: ys587
"""
from __future__ import print_function
import glob, os
import numpy as np
import pandas as pd
import logging
from numpy.random import RandomState
import time
from sklearn.cluster import MiniBatchKMeans, KMeans
import matplotlib.pyplot as plt
import sys
import re
import datetime as dt
fmt = '%Y%m%d'
import matplotlib.dates as mdates
import soundfile as sf


#regexSite = re.compile("S(\d{2})_SWIFT")
regexSite = re.compile("NZ01_S(\d{2})_2")
#regexDay = re.compile("S\d{2}_SWIFT\d{2}_(\d{8})")
regexDay = re.compile("_S\d{2}_(\d{8})")


def ReadSelFeaSite(DayList, DayListFea, DeciRatio, DeciPhase): #def ReadSelFea(WorkPath, DeciRatio, DeciPhase):
    #DayList = sorted(glob.glob(os.path.join(WorkPath+'/','*.txt')))
    #DayListFea = sorted(glob.glob(os.path.join(WorkPath+'/','*_Hog.npy')))
    SelMetaCurr = []
    SelFeaCurr = []
    #for dd in range(len(DayList)):
    DayInd = 0
    for dd in range(DeciPhase, len(DayList), DeciRatio):
    #for dd in range(5,10):
        if dd % 20 == 0:
            logging.warning('Day ' + str(dd)+' '+os.path.basename(DayList[dd]))
        PdCurr = pd.read_csv(DayList[dd], delimiter='\t')
        if PdCurr.shape[0] > 0:
            #PdCurr['Day'] = DayInd
            PdCurr['Date'] = dt.datetime.strptime(regexDay.search(os.path.basename(DayList[dd])).groups()[0], fmt)
            PdCurr['Site'] = int(regexSite.search(os.path.basename(DayList[dd])).groups()[0])-1
            SelMetaCurr.append(PdCurr)
            SelFeaCurr.append(np.load(DayListFea[dd]))
            #logging.warning(np.load(DayListFea[dd]).shape)
        DayInd += 1
        
    SelMetaTot = pd.concat(SelMetaCurr) # merge dataframe
    del SelMetaCurr
    SelMetaTot = SelMetaTot.reset_index() # reset index

    SelFeaTot = np.vstack(SelFeaCurr)
    del SelFeaCurr
    
    return SelMetaTot, SelFeaTot

def DielPlotSiteSoundCount(NumOfTimeSlots, NumDayYear, NumOfSec, SelDf, StartDay):
    SoundCount = np.zeros([NumOfTimeSlots, NumDayYear, NumSite])
    for ee in range(SelDf.shape[0]):
        if ee % 100000 == 0:
            logging.warning(ee)
        TimeStart = SelDf['Begin Time (s)'].iloc[ee]
        DayCurr = SelDf['YearDay'].iloc[ee] - StartDay
        SiteCurr = SelDf['Site'].iloc[ee]
        try:
            SoundCount[int(np.floor(TimeStart/NumOfSec)), int(DayCurr), int(SiteCurr)] += 1.0
        except:
            #print
            logging.warning(ee)
    return SoundCount

def MagAdjustedFea(SelFea, NumFreqBand): # reduce SelFea dimensions and put magnitue as weight on hog
    FeaMag = SelFea[:,-NumFreqBand:]
    SelFea = SelFea[:,:-NumFreqBand]
    
    for ss in range(SelFea.shape[0]):
        for tt in range(NumFreqBand):
            #SelFeaTot[ss,tt*NumBin:(tt+1)*NumBin] = SelFeaTot[ss,tt*NumBin:(tt+1)*NumBin]*SelFeaTot[ss,NumFreqBand*NumBin+tt]
            SelFea[ss,tt*NumBin:(tt+1)*NumBin] = SelFea[ss,tt*NumBin:(tt+1)*NumBin]*FeaMag[ss, tt]
    return SelFea

def SoundClipRead(SoundPath, TimeStart, TimeDelta):
    Fs = sf.info(SoundPath).samplerate
    f = sf.SoundFile(SoundPath, 'r')
    f.seek(int(TimeStart*Fs))
    SamplesCall = f.read(int(TimeDelta*Fs))
    return SamplesCall

def Dataframe2SelTab(DFInput, SelTabPath):
    f = open(SelTabPath,'w')
    f.write('Selection\tView\tChannel\tBegin Time (s)\tEnd Time (s)\tLow Freq (Hz)\tHigh Freq (Hz)\tScore\tBegin Path\tFile Offset (s)\tClass\tSite\n')
    for index, row in DFInput.iterrows():
        f.write(str(row[u'Selection'])+'\t'+'Spectrogram'+'\t'+str(row['Channel']+1)+'\t'+ \
                str.format("{0:=.4f}",row[u'Begin Time (s)']) + '\t' + str.format("{0:<.4f}",row[u'End Time (s)'])+ \
                '\t1600.0\t8000.0\t'+str.format("{0:<.4f}",row[u'Score'])+'\t'+row[u'Begin Path']+ \
                '\t'+str.format("{0:=.4f}",row[u'File Offset (s)'])+'\t'+str(row[u'Class'])+'\t'+str.format("{0:=.4f}",row[u'Site'])+'\n')
    f.close()
    return True

def ClassSiteHist(NumSite, NumClass, SelDf):
    SoundClassHist = np.zeros([NumClass, NumSite])
    for ee in range(SelDf.shape[0]):
        if ee % 100000 == 0:
            print(ee)
        SiteCurr = int(SelDf['Site'].iloc[ee])
        ClassCurr = int(SelDf['Class'].iloc[ee])
        SoundClassHist[ClassCurr, SiteCurr] += 1
    return SoundClassHist

def ClassDayHist(NumDay, NumClass, SelDf, StartDay):
    SoundClassHist = np.zeros([NumClass, NumDay])
    for ee in range(SelDf.shape[0]):
        if ee % 100000 == 0:
            print(ee)
        #SiteCurr = int(SelDf['Site'].iloc[ee])
        DayCurr = SelDf['YearDay'].iloc[ee] - StartDay
        ClassCurr = int(SelDf['Class'].iloc[ee])
        SoundClassHist[ClassCurr, DayCurr] += 1
    return SoundClassHist


import math
def nCr(n,r):
    f = math.factorial
    return f(n) / f(r) / f(n-r)


def DistWithinClass(ClusterDist, LabelPred, NumClass):    
    DistWithinClassInertia = np.zeros(NumClass)
    DistWithinClassAvg = np.zeros(NumClass)
    for TargetClass in range(NumClass):
        DistWithinClassInertia[TargetClass] =( (ClusterDist[LabelPred==TargetClass, TargetClass])**2.0).sum()
        DistWithinClassAvg[TargetClass] = np.sqrt(DistWithinClassInertia[TargetClass]/(LabelPred==TargetClass).sum())
    return DistWithinClassInertia, DistWithinClassAvg


def ClassSelTabOutputPerDaySite(SelMetaTot, SiteTarget, DateTarget, OutputPath, FileHead): # DateTarget in datetime format
    SelMetaDateSite = SelMetaTot.loc[(SelMetaTot['Site'] == SiteTarget) & (SelMetaTot['Date'] == DateTarget)] # Site & date
    SelTabPath = os.path.join(OutputPath,FileHead+str(SiteTarget+1)+'_'+str(DateTarget)+'.txt')
    Dataframe2SelTab(SelMetaDateSite, SelTabPath)
    return True


def ClassSelTabOutput(SelMetaTot, ClassTargetList, OutPath, Filehead, FeaName):
    if type(ClassTargetList) == int:
        ClassTargetList = [ClassTargetList]
    for ClassTarget0 in ClassTargetList:
        SelMetaTarget = SelMetaTot.loc[SelMetaTot['Class'] == ClassTarget0]
        SelTabPath = os.path.join(OutPath, Filehead+str(ClassTarget0)+'_'+FeaName+'.txt')
        if SelMetaTarget.shape[0]<=200:
            Dataframe2SelTab(SelMetaTarget, SelTabPath)
        else:
            Dataframe2SelTab(SelMetaTarget.sample(200), SelTabPath)
    return SelMetaTarget

def fancy_dendrogram(*args, **kwargs):
    max_d = kwargs.pop('max_d', None)
    if max_d and 'color_threshold' not in kwargs:
        kwargs['color_threshold'] = max_d
    annotate_above = kwargs.pop('annotate_above', 0)

    ddata = dendrogram(*args, **kwargs)

    if not kwargs.get('no_plot', False):
        plt.title('Hierarchical Clustering Dendrogram (truncated)')
        plt.xlabel('sample index or (cluster size)')
        plt.ylabel('distance')
        for i, d, c in zip(ddata['icoord'], ddata['dcoord'], ddata['color_list']):
            x = 0.5 * sum(i[1:3])
            y = d[1]
            if y > annotate_above:
                plt.plot(x, y, 'o', c=c)
                plt.annotate("%.3g" % y, (x, y), xytext=(0, -5),
                             textcoords='offset points',
                             va='top', ha='center')
        if max_d:
            plt.axhline(y=max_d, c='k')
    return ddata

def plot_embedding(X, Labels, title=None):
    x_min, x_max = np.min(X, 0), np.max(X, 0)
    #X = (X - x_min) / (x_max - x_min)
    plt.figure()
    ax = plt.subplot(111)
    XSpan = (x_max[0] - x_min[0])*0.2
    YSpan = (x_max[1] - x_min[1])*0.2
    ax.set_xlim(x_min[0]-XSpan, x_max[0]+XSpan) #!!
    ax.set_ylim(x_min[1]-YSpan, x_max[1]+YSpan) #!!
    for i in range(X.shape[0]):
        plt.text(X[i, 0], X[i, 1], Labels[i], fontdict={'weight': 'bold', 'size': 7})
    plt.grid()
    if title is not None:
        plt.title(title)
    plt.show()
        
def DigitStrZeroFormat(Digi, NumDigi):
    if(Digi>= 0):
        if(Digi < 10):
            DigiStrZero = '0'*(NumDigi-1)+str(Digi)
        elif(Digi < 100):
            DigiStrZero = '0'*(NumDigi-2)+str(Digi)
        elif(Digi < 1000):
            DigiStrZero = '0'*(NumDigi-3)+str(Digi)
        elif(Digi < 10000):
            DigiStrZero = '0'*(NumDigi-4)+str(Digi)
    else:
        DigiStrZero = r'String Needs to be positive.'
    return DigiStrZero

if __name__ == "__main__":
    #PROJ = 1 # HOG
    PROJ = 2 # MFCC

    DumpPath = r'P:\users\yu_shiu_ys587\__Soundscape\__ASA_Boston\__NewZealand'
    #DumpPath = u'P:\users\yu_shiu_ys587\__Soundscape\__ASA_Boston\__NewZealand'
    
    # Where the features are
    #WorkPath1 = r'/Volumes/Porter/__ASA_Boston/__SSW/S1067_SSW03_201702_Hog'
    #WorkPath1 = r'P:\users\yu_shiu_ys587\__Soundscape\__ASA_Boston\__NewZealand\NewZealand201612'
    #WorkPath1 = r'P:\users\yu_shiu_ys587\__Soundscape\__ASA_Boston\__NewZealand\NewZealand20171001'
    #WorkPath1 = r'P:\users\yu_shiu_ys587\__Soundscape\__ASA_Boston\__NewZealand\NewZealand20171018'
    WorkPath1 = r'P:\users\yu_shiu_ys587\__Soundscape\__ASA_Boston\__NewZealand\NewZealand20171025'
    #WorkPath1 = r'P:\users\yu_shiu_ys587\__Soundscape\__ASA_Boston\__NewZealand\NewZealandTemp'

    WorkPath1 = WorkPath1.replace('\\','/')
    #WorkPath2 = r'/Volumes/Porter/__ASA_Boston/__SSW/S1067_SSW04_201703_Hog'
    #WorkPath2 = r'P:\users\yu_shiu_ys587\__Soundscape\__ASA_Boston\__SSW\S1067_SSW04_201703_Hog'
    #WorkPath2 = WorkPath2.replace('\\','/')

    SegLength = 30 # in min        
    NumOfTimeSlots = int(24*(60./SegLength))
    NumOfSec = SegLength*60
        
    if PROJ == 1: # HOG
        FeaName = r'Hog'
        NumFreqBand = 20 # NumCellTime:2 ; NumCellFreq: 10
        NumBin = 18
        NumClass = 1000
        NumSite = 10    
        # Read day folders path
        DayList1 = sorted(glob.glob(os.path.join(WorkPath1+'/','*.txt')))
        DayListFea1 = sorted(glob.glob(os.path.join(WorkPath1+'/','*_Hog.npy')))
    elif PROJ == 2: # MFCC
        FeaName = r'Mfcc'
        DayList1 = sorted(glob.glob(os.path.join(WorkPath1+'/','*.txt')))
        DayListFea1 = sorted(glob.glob(os.path.join(WorkPath1+'/','*_MFCC.npy')))
        NumClass = 1000

    ## Read selection tables & features
    # Month 1
    logging.warning('Late Feb to Early Apr, 2017')
    #SelMetaTot1, SelFeaTot1 = ReadSelFeaSite(DayList1, DayListFea1, 30, 0)
    SelMetaTot1, SelFeaTot1 = ReadSelFeaSite(DayList1, DayListFea1, 1, 0) # read all
    
    logging.warning(' Revising features...')
    SelFeaTot = SelFeaTot1
    #SelFeaTot = np.vstack([SelFeaTot1, SelFeaTot2])
    del SelFeaTot1
    SelMetaTot = SelMetaTot1
    #SelMetaTot = pd.concat([SelMetaTot1, SelMetaTot2])
    del SelMetaTot1
    
    ## Magnitude adjusted
    if PROJ == 1: # HOG
        SelFeaTot = MagAdjustedFea(SelFeaTot, NumFreqBand)
    
    
    # load and classify
    # Clustering via Mini-batch
    logging.warning(' Minibatch kmeans...')
    
    # data normalization
    SelFeaTot0 = SelFeaTot
    for d in range(SelFeaTot0.shape[1]):
        TargetDim = SelFeaTot0[:,d]
        TargetDim = TargetDim - TargetDim.mean()
        SelFeaTot[:,d] = TargetDim/TargetDim.std()

    rng = RandomState(0)
    TimeStart = time.time()
    ## Minibatch kmeans
    ##NumClass = 1000 # temporary change
    #MiniBatchClass = MiniBatchKMeans(n_clusters=NumClass, tol=1e-3, batch_size=40, max_iter=100, random_state=rng)
    #MiniBatchClass = MiniBatchKMeans(n_clusters=NumClass, tol=1e-3, batch_size=40, max_iter=100, random_state=0)
    #MiniBatchClass = MiniBatchKMeans(n_clusters=NumClass, tol=1e-3, batch_size=40, max_iter=200, random_state=0)
    MiniBatchClass = MiniBatchKMeans(n_clusters=NumClass, tol=0.0, batch_size=40, random_state=0, verbose=1)
    #MiniBatchClass.fit(SelMetaTrain[:,:48])
        
    MiniBatchClass.fit(SelFeaTot)
    train_time = (time.time() - TimeStart)
    print("done in %0.3fs" % train_time)
    # get the centroid
    #FeaComponents = MiniBatchClass.cluster_centers_
    FeaCounts = MiniBatchClass.counts_

    # prediction: no need since compute_labels is true as default
    LabelPred =  MiniBatchClass.labels_
    SelMetaTot['Class'] = LabelPred
              
    ClusterDist = MiniBatchClass.transform(SelFeaTot)
    
    ############################################################################################################
    # Distance of Centroids
    Centroids = MiniBatchClass.cluster_centers_ 
    #Label = MiniBatchClass.labels_
    Inertia = MiniBatchClass.inertia_
    

    ############################################################################################################    
    ## histogram
    HistCount1, HistBin1 = np.histogram(LabelPred, bins=np.arange(-.5,float(NumClass)+.5, 1.0))
    plt.figure(); plt.plot(HistBin1[:-1]+0.5, HistCount1,'-+'); plt.grid(); plt.show()

    
    ############################################################################################################
    # Average distance of samples to their respective centroids
    DistWithinClassInertia, DistWithinClassAvg = DistWithinClass(ClusterDist, LabelPred, NumClass)

    #TargetClass = 2
    #DistWithinClassMean = np.zeros(NumClass)
    #DistWithinClassStd = np.zeros(NumClass)    
    #print 'Class: '+ str(TargetClass)
    #print 'Count: ' + str(HistCount1[TargetClass])
    #print np.sqrt((ClusterDist[Label==TargetClass]**2.).sum()/HistCount1[TargetClass])
    #print np.sqrt((ClusterDist[Label==TargetClass, TargetClass]**2.).sum()/HistCount1[TargetClass])
    #DistWithinClassMean[TargetClass] = (ClusterDist[LabelPred==TargetClass, TargetClass]).mean()
    #DistWithinClassStd[TargetClass] = (ClusterDist[LabelPred==TargetClass, TargetClass]).std()
    # inertia: sum of squares
    #DistWithinClassInertia[TargetClass] =( (ClusterDist[LabelPred==TargetClass, TargetClass])**2.0).sum()
    #DistWithinClassAvg[TargetClass] = np.sqrt(DistWithinClassInertia[TargetClass]/(LabelPred==TargetClass).sum())
    #plt.plot(DistWithinClassMean)
    #plt.plot(DistWithinClassStd)
    
    
    ############################################################################################################
    # Distance between centroids
    # empty class: 636, 648, 755, 904
    ############################################################################################################
    from scipy.spatial.distance import pdist, squareform
    DisMat2_Condensed = pdist(Centroids) # condensed distance matrix, given feature data
    DisMat2 = squareform(DisMat2_Condensed) # a distance matrix, given a redudant one
    
    if False:
        DisMat = np.zeros((NumClass, NumClass))
        for c1 in range(NumClass):
            for c2 in range(NumClass):
                DisMat[c1, c2] = np.sqrt(((Centroids[c1]-Centroids[c2])**2.).sum())
        DisMat[395, 229] # 83.177

    ############################################################################################################
    # Dendrogram
    # How to adjust threshold to get groups of clusters?
    
    from scipy.cluster.hierarchy import dendrogram, linkage
    
    #Z = linkage(Centroids, method='average')
    Z = linkage(DisMat2_Condensed, method='average')
    #Z = linkage(DisMat2, method='single')
    #Z = linkage(DisMat2, method='centroid')
    
    plt.figure()
    t1 = dendrogram(Z, leaf_font_size=8)
    plt.show()
    
    #!!
    # Task 1: DONE!!
    # Merge classes based on disance
    plt.figure()
    fancy_dendrogram(
        Z,
        truncate_mode='lastp',
        p=100,
        leaf_rotation=90.,
        leaf_font_size=10.,
        show_contracted=True,
        annotate_above=10,
        max_d=13.0,  # plot a horizontal cut-off line
    )
    plt.show()
    
    # Order class based on its distance with others
    # ClassIdDist
    # Given Z, output a matrix with class ID and distance in the decreasing order
    ClassCount = 0
    ClassIdDist = np.zeros((NumClass, 2))
    for cc in range(Z.shape[0]):
        TargetZ = Z[-(cc+1)]
        if(TargetZ[0]<NumClass):
            ClassIdDist[ClassCount, 0] = TargetZ[0]
            ClassIdDist[ClassCount, 1] = TargetZ[2]
            ClassCount += 1
        if(TargetZ[1]<NumClass):
            ClassIdDist[ClassCount, 0] = TargetZ[1]
            ClassIdDist[ClassCount, 1] = TargetZ[2]
            ClassCount += 1
        
            
    
    
    if False:
        from scipy.cluster.hierarchy import fcluster
        max_d = 10.0
        clusters = fcluster(Z, max_d, criterion='distance')
        clusters
    
    
    
    ############################################################################################################
    sys.exit()
    ############################################################################################################
    # Task 7: find class occurence between two habitats    
    HabitatClassCount = np.zeros((NumClass,2))
    OddList = np.zeros(NumClass)
    for cc in range(1000):
        print('Class %d' % cc)
        SelMetaClassHabitat1 = SelMetaTot.loc[(SelMetaTot['Site'] < 5) & (SelMetaTot['Class'] == cc)]
        SelMetaClassHabitat2 = SelMetaTot.loc[(SelMetaTot['Site'] >= 5) & (SelMetaTot['Class'] == cc)]
        HabitatClassCount[cc,0] = SelMetaClassHabitat1.shape[0]
        HabitatClassCount[cc,1] = SelMetaClassHabitat2.shape[0]
        if ((HabitatClassCount[cc,0]==0)&(HabitatClassCount[cc,1]==0)):
            OddList[cc] = 0.5
        else:
            OddList[cc] = HabitatClassCount[cc,0]/(HabitatClassCount[cc,0]+HabitatClassCount[cc,1])
        #print('In Class %d, the number of sound events for habitat mainland and island are %d and %d, respectively.' % (cc, SelMetaClassHabitat1.shape[0], SelMetaClassHabitat2.shape[0]))
    
    if False:
        plt.plot(OddList)
        (OddList>.8).sum()
        (OddList<.2).sum()
        (OddList<.5).sum()
        (OddList<=.5).sum()
        HabitatClassCount.sum(axis=1)
        HabitatClassCount.sum(axis=0)
        HistCount1[(OddList>.8)]
        HistCount1[(OddList>.8)].sum()
        HistCount1[(OddList<.2)].sum()
        HistCount1[(OddList>.75)].sum()
        HistCount1[(OddList<.25)].sum()
        248./1298
        91198./1297973
        HabitatClassCount[(OddList>.8)].sum()
        HabitatClassCount[(OddList>.8),0].sum()
        HabitatClassCount[(OddList>.8),1].sum()
        82595./1297973.
        HabitatClassCount[(OddList>.75),1].sum()
        HabitatClassCount[(OddList>.75),0].sum()    
    
    
    ############################################################################################################
    # Task 5: Done!
    # Add MFCC Delta T, i.e., time difference 
    ############################################################################################################

    

    
    # Task 3:
    # Do VQ!
    
    # Task 4:
    # Clustering based on time & VQ codes
    
    # Task:
    # Find class co-occurence
    

    



    ############################################################################################################
    # Output a selection table for each day-site with class labels on
    
    SiteTarget = 3 # SITE 4
    DateTarget = dt.date(2016, 12, 10)
    ClassSelTabOutputPerDaySite(SelMetaTot, SiteTarget, DateTarget, DumpPath,'NZ_Site_')
    
    # Task 6: generate selection table for all sites and all days
    DumpPath0 = os.path.join(DumpPath,'SelTab_AllSiteDay')
    if not os.path.exists(DumpPath0):
        os.makedirs(DumpPath0)
    for dd in range(10, 18+1):
        DateTarget = dt.date(2016, 12, dd)
        for ss in range(10):
            ClassSelTabOutputPerDaySite(SelMetaTot, ss, DateTarget, DumpPath0,'NZ_Site_')
            
    #SiteTarget = 0
    #DateTarget = dt.date(2016, 12, 10) 
    #SelMetaDateSite = SelMetaTot.loc[(SelMetaTot['Site'] == SiteTarget) & (SelMetaTot['Date'] == DateTarget)] # Site & date
    #SelTabPath = os.path.join(DumpPath,'NZ_Site_'+str(SiteTarget+1)+'_'+str(DateTarget)+'.txt')
    #Dataframe2SelTab(SelMetaDateSite, SelTabPath)
    
    
    ############################################################################################################
    # Task 2: Done!
    # Multiple Class Spectrogram display
    # Output all sound clips for each class, being limited to 200 events
    # Need to convert to function definition
    
    
    #DumpPath1 = os.path.join(DumpPath,'SelTabClass')
    #DumpPath1 = r'P:\users\yu_shiu_ys587\__Soundscape\__ASA_Boston\__NewZealand\__SelTabClass'
    DumpPath1 = r'P:\users\yu_shiu_ys587\__Soundscape\__ASA_Boston\__NewZealand'
    DumpPath2 = os.path.join(DumpPath, r'__SoundClipsTemp')

    DistThre = 8.0
    ClassListDistSort = (ClassIdDist[ClassIdDist[:,1]>DistThre])[:,0].astype(int).tolist()

    SampleNum = 25
    
    if not os.path.exists(DumpPath1):
        os.makedirs(DumpPath1)
    if not os.path.exists(DumpPath2):
        os.makedirs(DumpPath2)
    
    DistRank = 0
    #for cc in range(NumClass):
    for cc in ClassListDistSort:
    #for cc in range(10):
        # step 1: generate selection table for each class ==>> DONE!
        ##SelMetaClass = ClassSelTabOutput(SelMetaTot, cc, DumpPath1, 'NZ_Class_', FeaName)
        SelMetaClass = SelMetaTot.loc[SelMetaTot['Class'] == cc]
        
        # step 2: generate audio clips based on the dataframe for each class, given SelMetaClass
        
        #ClassStr = DigitStrZeroFormat(cc, 3):
        DistRankStr = DigitStrZeroFormat(DistRank, 3)
        #DumpPath3 = os.path.join(DumpPath2,r'Class'+str(cc))
        #if not os.path.exists(DumpPath3):
        #    os.makedirs(DumpPath3)
        
        if SelMetaClass.shape[0] <= SampleNum:
            SelMetaClass1 = SelMetaClass
            #del SelMetaClass
        else:
            SelMetaClass1 = SelMetaClass.sample(n=SampleNum)
            #del SelMetaClass
        
        for ee, MetaClass1 in SelMetaClass1.iterrows():
            Time1 = MetaClass1[u'File Offset (s)']
            TimeD = MetaClass1[u'End Time (s)'] - MetaClass1[u'Begin Time (s)']
            Sample0, Fs = sf.read(MetaClass1[u'Begin Path'])
            SampleStart = int(np.floor(Time1*Fs))
            SampleStop = int(np.floor((Time1 + TimeD)*Fs))
            Sample = Sample0[SampleStart:SampleStop]
            sf.write(os.path.join(DumpPath2, 'D'+DistRankStr+'_C'+str(cc)+'_E'+str(ee)+'.wav'), Sample, Fs)
        DistRank += 1
            
############################################################################################################
    # Tricks in soundfile to read blocks    
    #Fs = sfile.info(ff).samplerate
    #for Samples0 in sfile.blocks(ff, blocksize = int(SegLen*Fs)):
############################################################################################################
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    ############################################################################################################
    # Output a selection table given a class or a list of classes
    ClassTargetList0 = [348, 17]
    ClassSelTabOutput(SelMetaTot, ClassTargetList0, DumpPath, 'NZ_Class_', FeaName)
    
    ClassTargetList1 = [125, 82]
    ClassSelTabOutput(SelMetaTot, ClassTargetList1, DumpPath, 'NZ_Class_', FeaName)
    
    ClassTargetList2 = [39, 404, 617, 682]
    ClassSelTabOutput(SelMetaTot, ClassTargetList2, DumpPath, 'NZ_Class_', FeaName)
    
    ClassTargetList3 = [255, 38, 67, 769]
    ClassSelTabOutput(SelMetaTot, ClassTargetList3, DumpPath, 'NZ_Class_', FeaName)

    ClassTargetList4 = [253, 70, 108]
    ClassSelTabOutput(SelMetaTot, ClassTargetList4, DumpPath, 'NZ_Class_', FeaName)

    ClassTargetList5 = [75, 369, 645, 632, 99, 642] # 75 with 636, 369 with 648, 645 with 904, 632 with 755, 
    ClassSelTabOutput(SelMetaTot, ClassTargetList5, DumpPath, 'NZ_Class_', FeaName)

    ClassTargetList6 = [103, 793] 
    ClassSelTabOutput(SelMetaTot, ClassTargetList6, DumpPath, 'NZ_Class_', FeaName)
    ############################################################################################################
    # Output a selection table for each day-site with class labels on
    DateTarget = dt.date(2016, 12, 14)
    NumSite = 10
    for ss in range(NumSite):
        ClassSelTabOutputPerDaySite(SelMetaTot, ss, DateTarget, DumpPath,'NZ_Site_')
        
#    DateTarget = dt.date(2016, 12, 14) 
#    for SiteTarget in range(10):
#        SelMetaDateSite = SelMetaTot.loc[(SelMetaTot['Site'] == SiteTarget) & (SelMetaTot['Date'] == DateTarget)] # Site & date
#        
#        SelTabPath = os.path.join(DumpPath,'NZ_Site_'+str(SiteTarget+1)+'_'+str(DateTarget)+'.txt')
#        Dataframe2SelTab(SelMetaDateSite, SelTabPath)

    
    ############################################################################################################
    # Manifold!! Many kinds
    from sklearn import manifold
    
    labels = [str(i) for i in range(NumClass)]
    n_components = 2
    
    mds = manifold.MDS(n_components, max_iter=100, n_init=1)
    Y = mds.fit_transform(DisMat2)
    plot_embedding(Y, labels, 'MDS')
    
    # t-sne
    tsne = manifold.TSNE(n_components=2, init='pca', random_state=0)
    X_tsne = tsne.fit_transform(Centroids)
    plot_embedding(X_tsne, labels, 'T-SNE')
    
    # isomap
    n_neighbors = 30
    X_iso = manifold.Isomap(n_neighbors, n_components=2).fit_transform(Centroids)
    plot_embedding(X_iso, labels, "Isomap")
    
    # LLE
    clf = manifold.LocallyLinearEmbedding(n_neighbors, n_components=2, method='standard')
    X_lle = clf.fit_transform(Centroids)
    plot_embedding(X_lle, labels, "Locally Linear Embedding")


    ############################################################################################################
    # Output sound clips for all classes
    
    # Read 
    

    














    
    ## Given class, find the audio clips and write them into a selection table
    # generate selection table for sampled 200 sound clips
    if False:
        # np.where(HistCount1>60000)
        #ClassTarget = np.where(HistCount1 < 10)
        ClassTarget = np.argmax(HistCount1)
        #ClassTarget = 1 # 1st peak at 65; 2nd peak at 25
    
        #SelMetaTarget = SelMetaTot.loc[SelMetaTot['Class'] == ClassTarget]
        #SelTabPath = os.path.join(DumpPath,'NZ_Class_'+str(ClassTarget)+'.txt')
        #Dataframe2SelTab(SelMetaTarget.sample(200), SelTabPath)
        
        # write out one selection table for each target class
        ClassTarget = 414
        SelMetaTarget = SelMetaTot.loc[SelMetaTot['Class'] == ClassTarget]
        SelTabPath = os.path.join(DumpPath,'NZ_Class_'+str(ClassTarget)+'_'+FeaName+'.txt')
        #Dataframe2SelTab(SelMetaTarget, SelTabPath)
        Dataframe2SelTab(SelMetaTarget.sample(200), SelTabPath)

        ############################################################################################################
        # write out one selection table for each class in a list of target classes
        #ClassTargetList = [172, 112, 182, 439]
        #ClassTargetList = np.where(HistCount1 <= 10)[0].tolist()
        ClassTargetList = [79]
        #ClassTargetList = np.where(HistCount1>30000)[0].tolist()
        #ClassTargetList = np.where(HistCount1>30000)[0].tolist()
        #ClassTargetList = np.where( (HistCount1<30000) & (HistCount1>10000) )[0].tolist()
        print(ClassTargetList)
        for ClassTarget0 in ClassTargetList:
            SelMetaTarget = SelMetaTot.loc[SelMetaTot['Class'] == ClassTarget0]
            SelTabPath = os.path.join(DumpPath,'NZ_Class_'+str(ClassTarget0)+'_'+FeaName+'.txt')
            if SelMetaTarget.shape[0]<=200:
                Dataframe2SelTab(SelMetaTarget, SelTabPath)
            else:
                Dataframe2SelTab(SelMetaTarget.sample(200), SelTabPath)
                
        # Count < 10
    
    
    
        
    
    plt.figure()
    fancy_dendrogram(
        Z,
        truncate_mode='lastp',
        p=12,
        leaf_rotation=90.,
        leaf_font_size=12.,
        show_contracted=True,
        annotate_above=10,  # useful in small plots so annotations don't overlap
    )
    plt.show()
    
    plt.figure()
    fancy_dendrogram(
        Z,
        truncate_mode='lastp',
        p=50,
        leaf_rotation=90.,
        leaf_font_size=12.,
        show_contracted=True,
        annotate_above=10,  # useful in small plots so annotations don't overlap
    )
    plt.show()    
    
    plt.figure()
    fancy_dendrogram(
        Z,
        truncate_mode='lastp',
        p = 100,
        leaf_rotation=90.,
        leaf_font_size=10.,
        show_contracted=True,
        annotate_above=10,  # useful in small plots so annotations don't overlap
    )
    plt.show() 
    
    ClassTargetList6 = [378, 192] 
    ClassSelTabOutput(SelMetaTot, ClassTargetList6, DumpPath, 'NZ_Class_', FeaName)
    
    
    plt.figure()
    fancy_dendrogram(
        Z,
        truncate_mode='level',
        p = 6,
        leaf_rotation=90.,
        leaf_font_size=10.,
        show_contracted=True,
        annotate_above=10,  # useful in small plots so annotations don't overlap
    )
    plt.show() 
    
    plt.figure()
    fancy_dendrogram(
        Z1,
        truncate_mode='level',
        p = 6,
        leaf_rotation=90.,
        leaf_font_size=10.,
        show_contracted=True,
        annotate_above=10,  # useful in small plots so annotations don't overlap
    )
    plt.show() 

    
    
    
    
    
    
    

    
    
    
    







