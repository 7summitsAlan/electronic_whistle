# ELECTRONIC DOG WHISTLE #

This electronic dog whistle circuit is very simple to construct and is basically an emitter-coupled oscillator composed of T2 and T3. An squarewave voltage can be sampled from the collector of T3 (X2). This signal gives an oscillating character to the tone.

![dog_listening](https://bitbucket.org/7summitsAlan/electronic_whistle/raw/master/listening.jpg)

Without the squarewave signal, the sound produced by emitters of T2 and T3 (X4) has an annoying character.

An additional vibrato signal can be added to this basic sound through switch S1. The frequency of the vibrato is around 6 Hz. Its amplitude is determined by the R4. The value of R4 can vary from 100 up to 300KΩ but you can experiment with different values.

The organ keys can be made of either metal plates or etched printed circuit.
The trimmers P1 up to P8 adjust the pitch of each tone. The tones can be drastically changed by changing the value of C4.

## Circuit Diagram for the Electronic Whistle

![](https://bitbucket.org/7summitsAlan/electronic_whistle/raw/master/diagram.gif)


## Printed Circuit Board for the Electronic Whistle

![](https://bitbucket.org/7summitsAlan/electronic_whistle/raw/master/pcb.gif)

## Components Values
<pre>
R1 = R2 = 47K
R3 = 6.8K
R4 = 300K
R5 = 1.8K
R6 = 1.2K
R7 = 270
R8 = 5.6K
R9 = 560
R10 = 2.2K
P1 = P2 = P3 = P8 = 5K
P4 = P5 = P6 = P7 = 1K
C1 = 2µF/25V
C2 = C3 = 1µF/25V
C4 = 0.27µF
C5 = 4.7µF/25V
T1 = T2 = T3 = 2SC3622, 2SC3245, 2SC3248
</pre>
