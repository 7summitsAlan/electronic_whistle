#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""

V2 over V1: remove the image extration part, to reduce the memory usage

Extract images for learning representation in tensorflow
3 datasets: (i) __CHAOZX (ii) __NewZealand (iii) __SSW

cropped images / data augmentation / scale?

output: (i) selection table; (ii) images or waveform?

Goals: 
1. display long-term dataset
2. display short-term acoustic activity such as seiemic airgun on 20130902
periods of airgun pulses: 4 sec, 1 min. 
period of "gunshot" pulses: 3 min

Created on Tue Oct 24 14:10:31 2017

@author: atoultaro
"""
#from CHAOZ_ExtractImage_V0 import *
#from CHAOZ_ExtractImage_HOG_V0 import GetTimeStamp, SoundRead, PowerLawMatCal, FindIndexNonZero, universal_worker, pool_args, HogFeaCalc
#from pyswift_core import GetTimeStamp, SoundRead, PowerLawMatCal, FindIndexNonZero, universal_worker, pool_args, HogFeaCalc
from pyswift_core import GetTimeStamp, SoundRead, PowerLawMatCal, FindIndexNonZero, universal_worker, pool_args, MFCCFeaCalc, HogFeaCalc, MFCCTimeFeaCalc
import numpy as np
import glob, os
from math import pi as Pi
import logging
import scipy.signal as signal
import librosa
from multiprocessing import Pool #Process, Queue,
#import itertools
import time
import sys
import datetime

FlagExtHog = False
FlagExtMFCC = False
FlagExtSpec = False
# New: MFCC delta time
FlagExtMFCCTime = True
    
if (FlagExtHog or FlagExtMFCC or FlagExtSpec or FlagExtMFCCTime) == False:
    sys.exit()

logging.basicConfig(filename=r'P:\users\yu_shiu_ys587\__Soundscape\__ASA_Boston\__NewZealand\example.log', level=logging.INFO)

def detect_power_law(stream, seltab_path, config):
    print('Extract features from sound files...')
    sec_advance = float((config.N_read+config.N_previous)/stream.get_all_stream_fs()[0])
    
    timestamp_array =[]
    file_array = []
    counter = 0
    while True:                                
        try:
            # load the samples
            samps = stream.read(N = config.N_read, previousN=config.N_previous)[0]
            #advance the counter
            counter +=1

            # Append the timestamp 
            #timestamp = stream.get_current_timesamp() - datetime.timedelta(seconds=2.0)
            timestamp = stream.get_current_timesamp() - datetime.timedelta(seconds=sec_advance)
            
            # Set the current file
            current_file = stream.stream.filename
                        
            # current date
            #current_date = stream.get_current_timesamp().date()
    
            # Append the timestamp 
            timestamp_array.append(timestamp)
            
            # Set the current file
            file_array.append(current_file)                    
                
            #pre-process all the channels
            for ii in range(samps.shape[1]):
                #Spectro = preprocess(samps[:,ii], filter_args)
                Spectro = preprocess(samps[:,ii], config)
                SpectroList.append([Spectro])
            #SpectroList.append([Spectro]) # this seems weird: only save the feature of the last channel


        except (StreamGap, StreamEnd) as S:
            
            # If error was thrown after a read (as opposed to after writing
            # a new table, then read the samples and predict)          
            #ScoreArr = np.append(ScoreArr, 
            print('Make prediction')
            ScoreArr = make_predictions(model,
                                                 SpectroList, 
                                                 previous_channels,
                                                 config,
                                                 streamevent = True
                                                 )
            ScoreArr = ScoreArr.reshape(-1, previous_channels)
            ScoreArr = ScoreArr[:-1].reshape(-1, previous_channels)
            
            # save SpectroList
            print('Save features to numpy binary files...')
            np.savez(spectro_file_curr_day, SpectroList, file_array, timestamp_array)
                      
            # End of Stream write the dataframe
            if type(S).__name__ == 'StreamEnd' :
                # Make the raven table
                print('Make dataframe for selection tables')
                Raven_data_frame = make_raven_SndSel_table( 
                    ScoreArr, 
                    OverlapRatioThre,
                    file_array,
                    timestamp_array,
                    ScoreThre,
                    config,
                    date_format = date_format)

                #RavenTable_out = RavenTable_out.append(Raven_data_frame)
                
                #print('writing selection table ' + current_file)
                print('Write to the selection table')
                write_raven_table_to_CSV(Raven_data_frame,
                                         SelTabPathDetected,
                                         SelectionTableName,
                                         SNDSel= SNDSel)                    
                return False
        
            elif type(S).__name__ == 'StreamGap':
                # reset the list
                SpectroList = []
                file_array = []
                timestamp_array =[]
                counter = 0
                ScoreArr = []               
                #print('I passed a stream gap!!')
                continue
    
    
def DetectViaPowerLaw(DaySound, SelTabPath):
    DaySound = DaySound.replace('\\','/')
    #SoundList = sorted(glob.glob(os.path.join(DaySound+'/', '*.aif')))
    SoundList = sorted(glob.glob(os.path.join(DaySound+'/', '*.flac')))
#    SoundListTemp = glob.glob(os.path.join(DaySound+'/', '*.aif'))
#    if len(SoundListTemp) ==0:
#        SoundListTemp = glob.glob(os.path.join(DaySound+'/', '*.wav'))
#        if len(SoundListTemp) ==0:
#            return "Therea are no supported sound formats in .wav or .aif."
#    SoundList = sorted(SoundListTemp)     
    
    SelTabPath = SelTabPath.replace('\\','/')
    DayFile = os.path.join(SelTabPath+'/',os.path.basename(DaySound)+'.txt')
    
    MfccFeaList = []
    SpecFeaList = []
    HogFeaList = []
    MfccTimeFeaList = []
    #if not os.path.exists(DayFile):
    if True:
        f = open(DayFile,'w')
        f.write('Selection\tView\tChannel\tBegin Time (s)\tEnd Time (s)\tLow Freq (Hz)\tHigh Freq (Hz)\tScore\tDuration\tBegin Path\tFile Offset (s)')
        f.write('\n')
        EventId=0
        
        #ffCount = 0
        for ff in SoundList:
        #for ff in SoundList[28:48:10]:
        #for ff in SoundList[0:5]:
            ff2 = os.path.splitext(os.path.basename(ff))[0]
            ##print ff2
            logging.info(ff2)
            
            #if(ffCount == 29 or ffCount == 30):
            #    logging.info('Stop!!')
            
            # time stamp
            TimeCurr = GetTimeStamp(ff2)
            ##print TimeCurr
            logging.info(str(TimeCurr.hour)+':'+str(TimeCurr.minute)+':'+str(TimeCurr.second))
            # read sound file
            Samples0, SampleRate0 = SoundRead(ff)
            # resampling 
            if (SampleRate0 != SampleRateRef):
                Samples0 = signal.resample(Samples0, float(len(Samples0))/SampleRate0*SampleRateRef)
            
            # make single-channel sound multi-dimensional array
            if(Samples0.ndim == 1):
                #Samples0 = np.array([Samples0]).T
                Samples0 = Samples0[:,None]
            
            for cc in range(Samples0.shape[1]):            
            #for cc in range(2, 3): # channel number
                ##print "Channel "+str(cc)
                
                ss_last = int(np.floor(Samples0.shape[0]*1.0/SampleRateRef/SegLen))
                #for ss in range(ss_last+1): # fixed the problem on SampleRate. Use SampleRate0 mistakenly at first.
                for ss in range(ss_last): 
                #for ss in range(2):
                    #if ss == ss_last-1:
                    #    Samples = Samples0[ss*SegLen*SampleRateRef:,cc]
                    #else:    
                    #    Samples = Samples0[ss*SegLen*SampleRateRef:(ss+1)*SegLen*SampleRateRef,cc]
                    Samples = Samples0[ss*SegLen*SampleRateRef:(ss+1)*SegLen*SampleRateRef,cc]
                    Samples = Samples - Samples.mean()
                    
                    # STFT
                    SftfMat = np.abs(librosa.stft(Samples, n_fft=FFTSize, hop_length=HopSize))
                    PowerLawStft = PowerLawMatCal(SftfMat, Nu1, Nu2, Gamma) # over time, freq
                    ##TDetecFunc = PowerLawStft.sum(axis=0) # summation over all frequency!?
                    TDetecFunc = PowerLawStft[IndFreqLow:IndFreqHigh,:].sum(axis=0) # summation over all frequency!?
                    TDetecRange = (TDetecFunc > TDetecThre).astype(int)
                    
                    TDetecRangeDiff = np.diff(TDetecRange)
                    IndDiff, ValDiff = FindIndexNonZero(TDetecRangeDiff)
                    
                    EventList = []
                    if ValDiff: # test if there's any detection result
                        if(ValDiff[0] == -1):
                            ValDiff = [1] + ValDiff
                            IndDiff = [0] + IndDiff
                        if(ValDiff[-1] == 1):
                            ValDiff.append(-1)
                            IndDiff.append(len(TDetecRangeDiff))
                        # Feature extraction based on detection results
                        for tt in range(0, len(ValDiff), 2):
                            TimeE1 = IndDiff[tt]
                            TimeE2 = (IndDiff[tt+1]+1)
                            TimeE_Cen = int(np.floor((IndDiff[tt]+(IndDiff[tt+1]+1))/2.))                            
                            
                            #if (TDetecThreLen <= (TimeE2-TimeE1)*EventTimeReso) and (TimeE_Cen-TimeE_DurHalf >= 0) and (TimeE_Cen+TimeE_DurHalf<TDetecRangeDiff.shape[0]):
                            if (TDetecThreLen <= (TimeE2-TimeE1)*EventTimeReso) and (TimeE_Cen-TimeE_DurHalf >= 0) and (TimeE_Cen+TimeE_DurHalf<TDetecRangeDiff.shape[0]):
                            #if (TDetecThreLen <= (TimeE2-TimeE1)*EventTimeReso) and (TimeE_Cen >= 0) and (TimeE_Cen+TimeE_Dur<TDetecRangeDiff.shape[0]):
                                #EventTVal = np.max(TDetecFunc[IndDiff[tt]:(IndDiff[tt+1]+1)]) # This is the score! It is the max value in the detection function.
                                EventTVal = np.percentile(TDetecFunc[IndDiff[tt]:(IndDiff[tt+1]+1)], 75) # This is the score! It is the max value in the detection function.
                                #ImageVecCurr = PowerLawStft[IndFreqLow:IndFreqLow+ImgFreqDim,TimeE_Cen-TimeE_DurHalf:TimeE_Cen+TimeE_DurHalf]
                                #EventList.append([TimeE1,TimeE2,EventTVal, ImageVecCurr.flatten()])
                                PowerLawStftCurr = PowerLawStft[IndFreqLow:IndFreqHigh,TimeE_Cen-TimeE_DurHalf:TimeE_Cen+TimeE_DurHalf]
                                #PowerLawStftCurr = PowerLawStft[IndFreqLow:IndFreqHigh,TimeE_Cen:TimeE_Cen+TimeE_Dur]
                                
                                WavCurr = Samples[TimeE1*HopSize:TimeE2*HopSize]
                                ##print WavCurr.shape
                                #WavCurr = Samples[(TimeE_Cen-TimeE_DurHalf)*HopSize:(TimeE_Cen+TimeE_DurHalf)*HopSize]
                                
                                # Spectrogram image
                                #ImageVecCurr = PowerLawStftCurr
                                
                                # HoG feature extraction
                                if FlagExtHog:
                                    HogFeaCurr = HogFeaCalc(PowerLawStftCurr, NumCellFreq, NumCellTime)
                                else:
                                    HogFeaCurr = []
                                if FlagExtMFCC:
                                    MfccFeaCurr = MFCCFeaCalc(WavCurr, SampleRate0, NumMFCC, FreqHighMel)
                                else:
                                    MfccFeaCurr = []
                                if FlagExtMFCCTime:
                                    MfccTimeFeaCurr = MFCCTimeFeaCalc(WavCurr, SampleRate0, NumMFCC, FreqHighMel)                                
                                else:
                                    MfccTimeFeaCurr = []
                                if FlagExtSpec:
                                    EventStftPL0 = PowerLawStft[IndFreqLow:IndFreqHigh:4, TimeE_Cen-TimeE_DurHalf:TimeE_Cen+TimeE_DurHalf]
                                    EventStftPL = EventStftPL0.flatten()
                                else:
                                    EventStftPL = []
                                # write another script to extract image patches
                                # if the duration is long, break it down into several patches; if short, extract one
                                

                                ##print t1.shape
                                
                                #print MfccFeaCurr.shape
                                
                                #EventList.append([TimeE1,TimeE2,EventTVal, ImageVecCurr.flatten(), HogFeaCurr])
                                #EventList.append([TimeE1,TimeE2,EventTVal, [], HogFeaCurr])
                                #EventList.append([TimeE1,TimeE2,EventTVal, [], MfccFeaCurr])
                                #EventList.append([TimeE1,TimeE2,EventTVal, [], MfccFeaCurr, EventStftPL.flatten()])
                                ##EventList.append([TimeE1,TimeE2,EventTVal, HogFeaCurr, MfccFeaCurr, EventStftPL.flatten()])
                                EventList.append([TimeE1,TimeE2,EventTVal, HogFeaCurr, MfccFeaCurr, EventStftPL, MfccTimeFeaCurr])
                                
                        # write to selection table
                        for ee in range(len(EventList)):
                            # write to selection table
                            EventId += 1
                            Time1 = TimeCurr.hour*3600 + TimeCurr.minute*60.0 + TimeCurr.second + ss*SegLen*1.0 + EventList[ee][0]*EventTimeReso
                            Time2 = TimeCurr.hour*3600 + TimeCurr.minute*60.0 + TimeCurr.second + ss*SegLen*1.0 + EventList[ee][1]*EventTimeReso
                            TimeOffset = ss*SegLen*1.0 + EventList[ee][0]*EventTimeReso
                            #f.write(str(EventId)+'\t'+'Spectrogram'+'\t'+str(cc+1)+'\t'+str.format("{0:=.4f}",Time1)+'\t'+str.format("{0:<.4f}",Time2)+'\t'+str.format("{0:=.1f}",50.0)+'\t'+str.format("{0:=.1f}",800.0)+'\t'+str.format("{0:<.5f}",EventList[ee][2])+'\t'+str.format("{0:<.5f}",Time2-Time1)+'\t'+ff+'\t'+str.format("{0:=.4f}",TimeOffset) )
                            f.write(str(EventId)+'\t'+'Spectrogram'+'\t'+str(cc+1)+'\t'+str.format("{0:=.4f}",Time1)+'\t'+str.format("{0:<.4f}",Time2)+'\t'+str.format("{0:=.1f}",FreqLow)+'\t'+str.format("{0:=.1f}",FreqHigh)+'\t'+str.format("{0:<.5f}",EventList[ee][2])+'\t'+str.format("{0:<.5f}",Time2-Time1)+'\t'+ff+'\t'+str.format("{0:=.4f}",TimeOffset) )
                            
                            #f.write('Selection\tView\tChannel\tBegin Time (s)\tEnd Time (s)\tLow Freq (Hz)\tHigh Freq (Hz)\tScore\tBegin Path\tFile Offset (s)')
                            f.write('\n')
                            # collect HOG vectors
                            HogFeaList.append(EventList[ee][3])
                            # Collect MFCC features
                            MfccFeaList.append(EventList[ee][4])
                            # Collect STFT_PowerLaw features
                            SpecFeaList.append(EventList[ee][5])
                            # Collect MFCC time features
                            MfccTimeFeaList.append(EventList[ee][6])
            #ffCount += 1
        f.close()
        
        if FlagExtHog == True:
            ## save image file and write to numpy array files / .npy
            np.save(os.path.join(SelTabPath+'/',os.path.basename(DaySound)+'_Hog.npy'), np.array(HogFeaList))
            del HogFeaList
        if FlagExtMFCC == True:
            ## save hog file and write to numpy array files / .npy
            np.save(os.path.join(SelTabPath+'/',os.path.basename(DaySound)+'_MFCC.npy'), np.array(MfccFeaList))
            del MfccFeaList
        if FlagExtMFCCTime == True:
            ## save hog file and write to numpy array files / .npy
            np.save(os.path.join(SelTabPath+'/',os.path.basename(DaySound)+'_MFCCTime.npy'), np.array(MfccTimeFeaList))
            del MfccTimeFeaList
        if FlagExtSpec == True:
            ## save hog file and write to numpy array files / .npy
            np.save(os.path.join(SelTabPath+'/',os.path.basename(DaySound)+'_SpecPL.npy'), np.array(SpecFeaList))
            del SpecFeaList

        return os.path.basename(DaySound)+' was processed.'
    else:
        return os.path.basename(DaySound)+' already exists.'

if __name__ == "__main__":
    
    TEST_SNGLE_FILE = False # run in parallel for all days, all sites
    #TEST_SNGLE_FILE = True 
    SingleDep = True # True: single deployment; False: multiple deployment
    
    #SitePathBase = r'F:\S1068_NZ01_201612_UniformDays'
    #SitePathBase = r'N:\users\yu_shiu_ys587\__Soundscape\__NewZealandData\S1068_NZ01_201612_UniformDays'
    #SitePathBase = r'G:\S1068_NZ01_201612_UniformDays'
    SitePathBase = r'P:\projects\2016_BRP_IthacaNY_S1068\S1068_NZ01_201612\S1068_NZ01_TARU_FLAC'
    SelTabPath = r'P:\users\yu_shiu_ys587\__Soundscape\__ASA_Boston\__NewZealand\NewZealandTemp'
    
    DeployName = 'S1068NZ01'
    # Site Format SXX. E.g. S05, S10
    DayList = ['20161210', '20161211', '20161212', '20161213', '20161214', '20161215', '20161216', '20161217', '20161218']
    NumOfSite = 10
    
    # matrix holding all the day-site folder names
    # 2D list: NumOfSite xNumOfDay
    # Iterate to get all the filenames under a target folder
    if False:
        SiteDayList = [[None for x in range(len(DayList))] for y in range(NumOfSite)]
        
        for ss in range(NumOfSite):
            #if ss+1 != NumOfSite:\
            if ss < 10:
                SiteDayList[ss] = sorted(glob.glob(os.path.join(SitePathBase, DeployName+'_S0'+str(ss+1), '*')))
            else: #SiteNum==10
                SiteDayList[ss] = sorted(glob.glob(os.path.join(SitePathBase, DeployName+'_S'+str(ss+1), '*')))            
        SiteDayListFlat = [SiteDayList[ss][dd] for ss in range(NumOfSite) for dd in range(len(DayList))]
    
    SiteDayListFlat = []
    NumDay = len(DayList)
#    for ss in range(NumOfSite):
#        if ss+1 < 10:
#            FileSiteHead = DeployName+'_S0'+str(ss+1)
#        else:
#            FileSiteHead = DeployName+'_S'+str(ss+1)
#        for dd in range(NumDay):
#            #SiteDayListFlat[ss*NumDay+dd] = os.path.join(SitePathBase, DeployName+'_S0'+str(ss+1), DeployName+'_S0'+str(ss+1)+'_'+DayList[dd])
#            #SiteDayListFlat[ss*NumDay+dd] = os.path.join(SitePathBase, FileSiteHead, FileSiteHead+'_'+DayList[dd])
#            SiteDayListFlat.append(os.path.join(SitePathBase, FileSiteHead, FileSiteHead+'_'+DayList[dd]))
    for dd in range(NumDay):    
        for ss in range(NumOfSite):
            if ss+1 < 10:
                FileSiteHead = DeployName+'_S0'+str(ss+1)
            else:
                FileSiteHead = DeployName+'_S'+str(ss+1)
            SiteDayListFlat.append(os.path.join(SitePathBase, FileSiteHead, FileSiteHead+'_'+DayList[dd]))
            
    SelTabPathList = [SelTabPath] * len(SiteDayListFlat)
    
    # Select days to run
    #SiteDayListFlat = SiteDayListFlat
    #SiteDayListFlat = SiteDayListFlat[30:]
    #SiteDayListFlat = SiteDayListFlat[57:60]
    #SiteDayListFlat = SiteDayListFlat[:30] + SiteDayListFlat[71:]
    
    #SelTabPathList = SelTabPathList
    #SelTabPathList = SelTabPathList[30:]
    #SelTabPathList = SelTabPathList[57:60]
    #SelTabPathList = SelTabPathList[:30] + SelTabPathList[71:]
    
    #sys.exit()
    
    if TEST_SNGLE_FILE == True:
        #DetectViaPowerLaw(SiteDayListFlat[4], SelTabPathList[4])
        TheDay = -1
        print(DayList[TheDay])
        ReturnedStr = DetectViaPowerLaw(SiteDayListFlat[TheDay], SelTabPathList[TheDay])
        print(ReturnedStr)

    else:
        MyPool = Pool(4)    
        #MyPool = Pool(6)    
        try:
            for ReturnedStr in MyPool.imap_unordered(universal_worker, pool_args(DetectViaPowerLaw, SiteDayListFlat, SelTabPathList)):
                print(ReturnedStr)
            time.sleep(10)
        except KeyboardInterrupt:
            print("Caught KeyboardInterrupt, terminating workers")
            MyPool.terminate()
            MyPool.join()
        else:
            print("Work is finished. Quitting gracefully")
            MyPool.close()
            MyPool.join()
            