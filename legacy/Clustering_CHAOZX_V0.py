# -*- coding: utf-8 -*-
"""
k-means clustering on CHAOZX

mini-batch

Created on Thu Jun 22 11:08:16 2017

@author: ys587
"""
import glob, os
import numpy as np
import pandas as pd
import logging
from numpy.random import RandomState
import time
from sklearn.cluster import MiniBatchKMeans
import matplotlib.pyplot as plt
import soundfile as sf
#import librosa
import datetime as dt
import matplotlib.dates as mdates    

def ReadSelFea(DayList, DayListFea, DeciRatio, DeciPhase): #def ReadSelFea(WorkPath, DeciRatio, DeciPhase):
    #DayList = sorted(glob.glob(os.path.join(WorkPath+'/','*.txt')))
    #DayListFea = sorted(glob.glob(os.path.join(WorkPath+'/','*_Hog.npy')))
    SelMetaCurr = []
    SelFeaCurr = []
    #for dd in range(len(DayList)):
    DayInd = 0
    for dd in range(DeciPhase, len(DayList), DeciRatio):
    #for dd in range(5,10):
        if dd % 20 == 0:
            logging.warning('Day ' + str(dd)+' '+os.path.basename(DayList[dd]))
        PdCurr = pd.read_csv(DayList[dd], delimiter='\t')
        if PdCurr.shape[0] > 0:
            PdCurr['Day'] = DayInd
            SelMetaCurr.append(PdCurr)
            SelFeaCurr.append(np.load(DayListFea[dd]))
            #logging.warning(np.load(DayListFea[dd]).shape)
        DayInd += 1
        
    SelMetaTot = pd.concat(SelMetaCurr) # merge dataframe
    del SelMetaCurr
    SelMetaTot = SelMetaTot.reset_index() # reset index

    SelFeaTot = np.vstack(SelFeaCurr)
    del SelFeaCurr
    
    return SelMetaTot, SelFeaTot
    
#def ReadSelFea(WorkPath, DeciRatio, DeciPhase):
#    DayList = sorted(glob.glob(os.path.join(WorkPath+'/','*.txt')))
#    DayListFea = sorted(glob.glob(os.path.join(WorkPath+'/','*_Hog.npy')))
#    SelMetaCurr = []
#    SelFeaCurr = []
#    #for dd in range(len(DayList)):
#    for dd in range(DeciPhase, len(DayList), DeciRatio):
#    #for dd in range(5,10):
#        logging.warning('Day ' + str(dd)+' '+os.path.basename(DayList[dd]))
#        PdCurr = pd.read_csv(DayList[dd], delimiter='\t')
#        if PdCurr.shape[0] > 0:
#            SelMetaCurr.append(PdCurr)
#            SelFeaCurr.append(np.load(DayListFea[dd]))
#            #logging.warning(np.load(DayListFea[dd]).shape)
#        
#    SelMetaTot = pd.concat(SelMetaCurr) # merge dataframe
#    del SelMetaCurr
#    SelMetaTot = SelMetaTot.reset_index() # reset index
#
#    SelFeaTot = np.vstack(SelFeaCurr)
#    del SelFeaCurr
#    
#    return SelMetaTot, SelFeaTot

def MagAdjustedFea(SelFea, NumFreqBand): # reduce SelFea dimensions and put magnitue as weight on hog
    FeaMag = SelFea[:,-NumFreqBand:]
    SelFea = SelFea[:,:-NumFreqBand]
    
    for ss in range(SelFea.shape[0]):
        for tt in range(NumFreqBand):
            #SelFeaTot[ss,tt*NumBin:(tt+1)*NumBin] = SelFeaTot[ss,tt*NumBin:(tt+1)*NumBin]*SelFeaTot[ss,NumFreqBand*NumBin+tt]
            SelFea[ss,tt*NumBin:(tt+1)*NumBin] = SelFea[ss,tt*NumBin:(tt+1)*NumBin]*FeaMag[ss, tt]
    return SelFea

def SoundClipRead(SoundPath, TimeStart, TimeDelta):
    Fs = sf.info(SoundPath).samplerate
    f = sf.SoundFile(SoundPath, 'r')
    f.seek(int(TimeStart*Fs))
    SamplesCall = f.read(int(TimeDelta*Fs))
    return SamplesCall

def Dataframe2SelTab(DFInput, SelTabPath):
    f = open(SelTabPath,'w')
    f.write('Selection\tView\tChannel\tBegin Time (s)\tEnd Time (s)\tLow Freq (Hz)\tHigh Freq (Hz)\tScore\tBegin Path\tFile Offset (s)\tClass\tDay\n')
    for index, row in DFInput.iterrows():
        f.write(str(row[u'Selection'])+'\t'+'Spectrogram'+'\t'+str(row['Channel']+1)+'\t'+ \
                str.format("{0:=.4f}",row[u'Begin Time (s)']) + '\t' + str.format("{0:<.4f}",row[u'End Time (s)'])+ \
                '\t200.0\t800.0\t'+str.format("{0:<.4f}",row[u'Score'])+'\t'+row[u'Begin Path']+ \
                '\t'+str.format("{0:=.4f}",row[u'File Offset (s)'])+'\t'+str.format("{0:=.4f}",row[u'Class'])+'\t'+str.format("{0:=.4f}",row[u'Day'])+'\n')
    f.close()
    return True

def DielPlotSoundCount(NumOfTimeSlots, NumDayYear, NumOfSec, SelDf):
    SoundCount = np.zeros([NumOfTimeSlots, NumDayYear])
    for ee in range(SelDf.shape[0]):
        if ee % 100000 == 0:
            logging.warning(ee)
        #TimeStart = SelDf['Begin Time (s)'][ee]
        TimeStart = SelDf['Begin Time (s)'].iloc[ee]
        #DayCurr = SelDf['Day'][ee]
        DayCurr = SelDf['Day'].iloc[ee]
        SoundCount[int(np.floor(TimeStart/NumOfSec)), int(DayCurr)] += 1.0
    return SoundCount

def DielPlotDominantClass(NumOfTimeSlots, NumDayYear, NumOfSec, SelDf, NumClass):
    SoundClass = np.zeros([NumOfTimeSlots, NumDayYear, NumClass])
    for ee in range(SelDf.shape[0]):
        if ee % 100000 == 0:
            logging.warning(ee)
        #TimeStart = SelDf['Begin Time (s)'][ee]
        TimeStart = SelDf['Begin Time (s)'].iloc[ee]
        #DayCurr = SelDf['Day'][ee]
        DayCurr = SelDf['Day'].iloc[ee]
        #ClassCurr = SelDf['Class'][ee]
        ClassCurr = SelDf['Class'].iloc[ee]
        SoundClass[int(np.floor(TimeStart/NumOfSec)), int(DayCurr), ClassCurr] += 1.0
    
    SoundClassDominant = np.zeros([NumOfTimeSlots, NumDayYear])
    for tt in range(NumOfTimeSlots):
        for dd in range(NumDayYear):
            SoundClassDominant[tt, dd] = np.argmax(SoundClass[tt,dd,:])
    return SoundClassDominant

def ClassHistPerDay(NumOfTimeSlots, NumDayYear, NumOfSec, SelDf, NumClass):
    SoundClassHist = np.zeros([NumClass, NumDayYear])
    for ee in range(SelDf.shape[0]):
        if ee % 100000 == 0:
            print ee
        DayCurr = SelDf['Day'].iloc[ee]
        ClassCurr = SelDf['Class'].iloc[ee]
        SoundClassHist[ClassCurr, DayCurr] += 1
    return SoundClassHist

if __name__ == "__main__":
    PROJ = 1 # CHAOZ-X
    #PROJ = 2 # NZ
    #PROJ = 3 # SSW

    DumpPath = r'P:\users\yu_shiu_ys587\__Soundscape\__ASA_Boston\__CHAOZX'
    
    WorkPath1 = r'P:\users\yu_shiu_ys587\__Soundscape\__ASA_Boston\__CHAOZX\CHAOZX_Hog_2013'
    #WorkPath1 = r'/Volumes/Porter/__ASA_Boston/__CHAOZX/CHAOZX_Hog_2013'
    WorkPath1 = WorkPath1.replace('\\','/')
    WorkPath2 = r'P:\users\yu_shiu_ys587\__Soundscape\__ASA_Boston\__CHAOZX\CHAOZX_Hog_2014'
    #WorkPath2 = r'/Volumes/Porter/__ASA_Boston/__CHAOZX/CHAOZX_Hog_2014'
    WorkPath2 = WorkPath2.replace('\\','/')
    
    SegLength = 30 # in min
    NumOfTimeSlots = int(24*(60./SegLength))
    NumOfSec = SegLength*60
        
    if PROJ == 1: #CHAOXZ: 8 * 18 + 8
        NumFreqBand = 8
        NumBin = 18
        FFTSize = 512
        HopSize = 200
        NumClass = 100
    elif PROJ == 2:
        NumFreqBand = 8
        NumBin = 18
    elif PROJ == 3:
        NumFreqBand = 8
        NumBin = 18
        
    # Read day folders path
    DayList1 = sorted(glob.glob(os.path.join(WorkPath1+'/','*.txt')))
    DayListFea1 = sorted(glob.glob(os.path.join(WorkPath1+'/','*_Hog.npy')))
    DayList2 = sorted(glob.glob(os.path.join(WorkPath2+'/','*.txt')))
    DayListFea2 = sorted(glob.glob(os.path.join(WorkPath2+'/','*_Hog.npy')))

    ## Read selection tables & features
    # Year 1
    logging.warning('Reading feature from Year 1 for clustering')
    #SelMetaTot1, SelFeaTot1 = ReadSelFea(WorkPath1, 30, 11)
    #SelMetaTot1, SelFeaTot1 = ReadSelFea(WorkPath1, 4, 11)
    #SelMetaTot1, SelFeaTot1 = ReadSelFea(DayList0, DayListFea0, 30, 11)
    SelMetaTot1, SelFeaTot1 = ReadSelFea(DayList1, DayListFea1, 4, 11)
    
    
    logging.warning('Reading feature from Year 2 for clustering')
    #SelMetaTot2, SelFeaTot2 = ReadSelFea(WorkPath2, 30, 11)
    #SelMetaTot2, SelFeaTot2 = ReadSelFea(WorkPath2, 4, 11)
    #SelMetaTot2, SelFeaTot2 = ReadSelFea(DayList0, DayListFea0, 30, 11)
    SelMetaTot2, SelFeaTot2 = ReadSelFea(DayList2, DayListFea2, 4, 11)
    
    logging.warning(' Revising features...')
    SelFeaTot = np.vstack([SelFeaTot1, SelFeaTot2])
    del SelFeaTot1, SelFeaTot2
    SelMetaTot = pd.concat([SelMetaTot1, SelMetaTot2])
    del SelMetaTot1, SelMetaTot2
    SelFeaTot = MagAdjustedFea(SelFeaTot, NumFreqBand) # HOG features adjusted by magnitude
    
#    FeaMagTot = SelFeaTot[:,-NumFreqBand:]
#    SelFeaTot = SelFeaTot[:,:-NumFreqBand]
#    
#    for ss in range(SelFeaTot.shape[0]):
#        for tt in range(NumFreqBand):
#            #SelFeaTot[ss,tt*NumBin:(tt+1)*NumBin] = SelFeaTot[ss,tt*NumBin:(tt+1)*NumBin]*SelFeaTot[ss,NumFreqBand*NumBin+tt]
#            SelFeaTot[ss,tt*NumBin:(tt+1)*NumBin] = SelFeaTot[ss,tt*NumBin:(tt+1)*NumBin]*FeaMagTot[ss, tt]
    
    # load and classify
    # Clustering via Mini-batch
    logging.warning(' Minibatch kmeans...')
    
    rng = RandomState(0)
    TimeStart = time.time()
    
    ## Minibatch kmeans
    ##NumClass = 1000 # temporary change
    #MiniBatchClass = MiniBatchKMeans(n_clusters=NumClass, tol=1e-3, batch_size=40, max_iter=100, random_state=rng)
    MiniBatchClass = MiniBatchKMeans(n_clusters=NumClass, tol=1e-3, batch_size=40, max_iter=100, random_state=0)
    #MiniBatchClass.fit(SelMetaTrain[:,:48])
    
    MiniBatchClass.fit(SelFeaTot)
    train_time = (time.time() - TimeStart)
    print("done in %0.3fs" % train_time)
    # get the centroid
    FeaComponents = MiniBatchClass.cluster_centers_
    FeaCounts = MiniBatchClass.counts_
    
    # prediction on Year 1 & 2
    #ClassPred = MiniBatchClass.predict(SelFeaTot)
    logging.warning('Reading feature from Year 1 for prediction')
    #SelMetaTest1, SelFeaTest1 = ReadSelFea(WorkPath1, 30, 11)
    #SelMetaTest1, SelFeaTest1 = ReadSelFea(WorkPath1, 1, 0)
    DayList1 = DayList1[:-1]
    DayListFea1 = DayListFea1[:-1]
    SelMetaTest1, SelFeaTest1 = ReadSelFea(DayList1, DayListFea1, 1, 0)

    logging.warning('Reading feature from Year 2 for prediction')
    #SelMetaTest2, SelFeaTest2 = ReadSelFea(WorkPath2, 30, 11)
    #SelMetaTest2, SelFeaTest2 = ReadSelFea(WorkPath2, 1, 0)
    DayList2 = DayList2[21:]
    DayListFea2 = DayListFea2[21:]
    SelMetaTest2, SelFeaTest2 = ReadSelFea(DayList2, DayListFea2, 1, 0) # start Aug 20
    
    # HOG features adjusted by magnitude
    logging.warning(' Revising features...')
    SelFeaTest1 = MagAdjustedFea(SelFeaTest1, NumFreqBand)
    SelFeaTest2 = MagAdjustedFea(SelFeaTest2, NumFreqBand)
    
    # Prediction
    logging.warning('Prediction for year 1')    
    ClassPred1 = MiniBatchClass.predict(SelFeaTest1)
    logging.warning('Prediction for year 2')
    ClassPred2 = MiniBatchClass.predict(SelFeaTest2)
    SelMetaTest1['Class'] = ClassPred1
    SelMetaTest2['Class'] = ClassPred2
                
    HistCount1, HistBin1 = np.histogram(ClassPred1, bins=np.arange(-.5,float(NumClass)+.5, 1.0))
    HistCount2, HistBin2 = np.histogram(ClassPred2, bins=np.arange(-.5,float(NumClass)+.5, 1.0))
    plt.figure(); plt.plot(HistBin1[:-1]+0.5, HistCount1,'-+'); plt.plot(HistBin2[:-1]+0.5, HistCount2,'-+'); plt.grid(); plt.show()
    
    if False:
        SelMetaTest1.to_csv(os.path.join(DumpPath,'SelMetaCHAOZX_1.csv'))
        #SelMetaTest1=pd.read_csv(os.path.join(DumpPath,'SelMetaCHAOZX_1.csv'))
        SelMetaTest2.to_csv(os.path.join(DumpPath,'SelMetaCHAOZX_2.csv'))
        #SelMetaTest2=pd.read_csv(os.path.join(DumpPath,'SelMetaCHAOZX_2.csv'))

    ########################
    # Figures & Applications
    ########################
    
    # Given class, find the audio clips and write them into a selection table
    if False:
        ClassTarget = 6
        SelMetaTarget = SelMetaTest1.loc[SelMetaTest1['Class'] == ClassTarget]
        #SelTabPath = r'P:\users\yu_shiu_ys587\__Soundscape\__ASA_Boston\__CHAOZX\temp.txt'
        SelTabPath = os.path.join(DumpPath,'CHAOZX_Class'+str(ClassTarget)+'.txt')
        Dataframe2SelTab(SelMetaTarget.sample(50), SelTabPath)
        #Dataframe2SelTab(SelMetaTarget.tail(20), SelTabPath)
        
    # show sound count
    NumDayYear1 = int(SelMetaTest1['Day'].max())+1
    NumDayYear2 = int(SelMetaTest2['Day'].max())+1
                     
#    SoundCount = np.zeros([NumOfTimeSlots, NumDayYear1]) # time slots per day x num of days
#    # Dominant class
#    DominantClass = np.zeros([NumOfTimeSlots, NumDayYear1])
#    for dd in range(NumDayYear1):
#        print 'Day: %d' % dd
#        SoundCurrDay = SelMetaTest1Df.ix[SelMetaTest1Df[r'Day'] == dd]
#        print SoundCurrDay.shape[0]

    #### Calculation ####
    ## Sound Count
    #SoundCount1, DominantClass1 = DielPlotSound(NumOfTimeSlots, NumDayYear1, NumOfSec, SelMetaTest1Df)
    #SoundCount2, DominantClass2 = DielPlotSound(NumOfTimeSlots, NumDayYear2, NumOfSec, SelMetaTest2Df)
    SoundCount1 = DielPlotSoundCount(NumOfTimeSlots, NumDayYear1, NumOfSec, SelMetaTest1)
    SoundCount2 = DielPlotSoundCount(NumOfTimeSlots, NumDayYear2, NumOfSec, SelMetaTest2)
    
    DateStart = dt.datetime(2013, 8, 20)
    DateStop = dt.datetime(2014, 8, 04)

    ## Sound Count
    fig1, axis1 = plt.subplots(nrows=2, ncols=1, figsize=[18, 8])
    XYLim = [mdates.date2num(DateStart), mdates.date2num(DateStop), 0, 24]
    im = axis1[0].imshow(SoundCount1[:,0:350], extent=XYLim, origin='lower', aspect='auto', cmap='jet', vmax=600, vmin=0)
    im = axis1[1].imshow(SoundCount2[:,0+21:350+21], extent=XYLim, origin='lower', aspect='auto', cmap='jet', vmax=600, vmin=0)
    
    axis1[0].xaxis.set_major_locator(mdates.MonthLocator( interval=1))
    axis1[0].set_xticklabels([])    
    axis1[1].xaxis.set_major_locator(mdates.MonthLocator( interval=1))
    #axis1[1].xaxis.set_major_formatter(mdates.DateFormatter('%d-%b-%Y'))
    axis1[1].xaxis.set_major_formatter(mdates.DateFormatter('%d-%b'))
    axis1[1].xaxis_date()
    axis1[1].autoscale_view()
    axis1[1].xaxis.tick_bottom()
    plt.setp(plt.gca().get_xticklabels(), rotation=30, fontsize=10)

    fig1.subplots_adjust(right=0.8)
    cbar_ax = fig1.add_axes([0.85, 0.15, 0.05, 0.7])
    cb1 = fig1.colorbar(im, cax=cbar_ax)
    cb1.ax.set_title('Count of Sound Event', fontdict={'fontsize':10})
    
    axis1[0].set_title('DB13_01: 20-Aug-2013 - 04-Aug-2014')
    axis1[0].set_ylabel('Hour of the Day')
    axis1[1].set_title('DB14_01: 20-Aug-2014 - 04-Aug-2015')
    axis1[1].set_ylabel('Hour of the Day')
    plt.show()
    
    ## Sound Class: diel plot of dominant class
    SoundDomiClass1 = DielPlotDominantClass(NumOfTimeSlots, NumDayYear1, NumOfSec, SelMetaTest1, NumClass)
    SoundDomiClass2 = DielPlotDominantClass(NumOfTimeSlots, NumDayYear2, NumOfSec, SelMetaTest2, NumClass)
    #
    fig3, axis3 = plt.subplots(nrows=2, ncols=1, figsize=[18, 12])
    XYLim = [mdates.date2num(DateStart), mdates.date2num(DateStop), 0, 24]
    im = axis3[0].imshow(SoundDomiClass1[:,0:350], extent=XYLim, origin='lower', aspect='auto', cmap='jet', vmax=99, vmin=0)
    im = axis3[1].imshow(SoundDomiClass2[:,0+21:350+21], extent=XYLim, origin='lower', aspect='auto', cmap='jet', vmax=99, vmin=0)
    
    axis3[0].xaxis.set_major_locator(mdates.MonthLocator( interval=1))
    axis3[0].set_xticklabels([])    
    axis3[1].xaxis.set_major_locator(mdates.MonthLocator( interval=1))
    axis3[1].xaxis.set_major_formatter(mdates.DateFormatter('%d-%b'))
    axis3[1].xaxis_date()
    axis3[1].autoscale_view()
    axis3[1].xaxis.tick_bottom()
    plt.setp(plt.gca().get_xticklabels(), rotation=30, fontsize=10)

    fig3.subplots_adjust(right=0.8)
    cbar_ax = fig3.add_axes([0.85, 0.15, 0.05, 0.7])
    cb3 = fig3.colorbar(im, cax=cbar_ax)
    cb3.ax.set_title('Dominant Sound Class', fontdict={'fontsize':10})
    
    axis3[0].set_title('DB13_01: 20-Aug-2013 - 04-Aug-2014')
    axis3[0].set_ylabel('Hour of the Day')
    axis3[1].set_title('DB14_01: 20-Aug-2014 - 04-Aug-2015')
    axis3[1].set_ylabel('Hour of the Day')
    plt.show()
    
#    ## Sound Class: class hist vs day
    SoundClassHist1 = ClassHistPerDay(NumOfTimeSlots, NumDayYear1, NumOfSec, SelMetaTest1, NumClass)
    SoundClassHist2 = ClassHistPerDay(NumOfTimeSlots, NumDayYear2, NumOfSec, SelMetaTest2, NumClass)
    #
    VMax = max(SoundClassHist1.flatten().max(), SoundClassHist2.flatten().max())
    VMax = 0.2*VMax
    Colormap0 = 'jet'
    fig5, axis5 = plt.subplots(nrows=2, ncols=1, figsize=[18, 12])
    XYLim = [mdates.date2num(DateStart), mdates.date2num(DateStop), 1, 100]
    im = axis5[0].imshow(SoundClassHist1[:,0:350], extent=XYLim, origin='lower', cmap=Colormap0, vmin=0, vmax=VMax)
    im = axis5[1].imshow(SoundClassHist2[:,21:371], extent=XYLim, origin='lower', cmap=Colormap0, vmin=0, vmax=VMax)
    
    axis5[0].xaxis.set_major_locator(mdates.MonthLocator( interval=1))
    axis5[0].set_xticklabels([])    
    axis5[1].xaxis.set_major_locator(mdates.MonthLocator( interval=1))
    axis5[1].xaxis.set_major_formatter(mdates.DateFormatter('%d-%b-%Y'))
    axis5[1].xaxis_date()
    axis5[1].autoscale_view()
    axis5[1].xaxis.tick_bottom()
    plt.setp(plt.gca().get_xticklabels(), rotation=30, fontsize=10)
    
    fig5.subplots_adjust(right=0.8)
    cbar_ax = fig5.add_axes([0.85, 0.15, 0.05, 0.7])
    cb5 = fig5.colorbar(im, cax=cbar_ax)
    cb5.ax.set_title('Histogram of Sound Classes', fontdict={'fontsize':10})
    
    axis5[0].set_title('DB13_01: 20-Aug-2013 - 04-Aug-2014')
    axis5[0].set_ylabel('Class Index')
    axis5[1].set_title('DB14_01: 20-Aug-2014 - 04-Aug-2015')
    axis5[1].set_ylabel('Class Index')
    plt.show()
    
#    ## Sound Class: class hist vs hours
#    SoundClassHistHour1 = ClassHistPerHour(NumOfTimeSlots, NumDayYear1, NumOfSec, SelMetaTest1Df, NumClass)
#    SoundClassHistHour2 = ClassHistPerHour(NumOfTimeSlots, NumDayYear2, NumOfSec, SelMetaTest2Df, NumClass)
#    
#    
#    


    
#   retrieve calls
#    for Index, Row in SelMetaTarget.iterrows():
#        SamplesCall = SoundClipRead(Row[u'Begin Path'], Row[u'File Offset (s)'], Row[u'End Time (s)']-Row[u'Begin Time (s)'])
#        #plt.plot(SamplesCall); plt.show()
#        SpectroCall = abs(librosa.stft(SamplesCall, FFTSize, HopSize, FFTSize))
#        plt.imshow(SpectroCall); plt.show()


#    FeaMagTest = SelFeaTest[:,-NumFreqBand:]
#    SelFeaTest = SelFeaTest[:,:-NumFreqBand]
#    
#    for ss in range(SelFeaTest.shape[0]):
#        for tt in range(NumFreqBand):
#            #SelFeaTot[ss,tt*NumBin:(tt+1)*NumBin] = SelFeaTot[ss,tt*NumBin:(tt+1)*NumBin]*SelFeaTot[ss,NumFreqBand*NumBin+tt]
#            SelFeaTest[ss,tt*NumBin:(tt+1)*NumBin] = SelFeaTest[ss,tt*NumBin:(tt+1)*NumBin]*FeaMagTest[ss, tt]

            
#    FeaMagTest = SelFeaTest[:,-NumFreqBand:]
#    SelFeaTest = SelFeaTest[:,:-NumFreqBand]
#    
#    for ss in range(SelFeaTest.shape[0]):
#        for tt in range(NumFreqBand):
#            #SelFeaTot[ss,tt*NumBin:(tt+1)*NumBin] = SelFeaTot[ss,tt*NumBin:(tt+1)*NumBin]*SelFeaTot[ss,NumFreqBand*NumBin+tt]
#            SelFeaTest[ss,tt*NumBin:(tt+1)*NumBin] = SelFeaTest[ss,tt*NumBin:(tt+1)*NumBin]*FeaMagTest[ss, tt]

    
## Decomposition
#    if False:
#        logging.warning(' Minibatch dictionary learning...')
#        NumDict = 100
#        from sklearn.decomposition import MiniBatchDictionaryLearning
#        MiniBatchDict = MiniBatchDictionaryLearning(n_components=NumDict)
#        MiniBatchDict.fit(SelFeaTot)
#        # plt.plot(MiniBatchDict.components_[10],'-+'); plt.grid()
#        DictComp = MiniBatchDict.transform(SelFeaTot) # how about using those dictionary items to do kmeans?
    
    
    # read both selection tables and features
#    DayList = sorted(glob.glob(os.path.join(WorkPath+'/','*.txt')))
#    DayListFea = sorted(glob.glob(os.path.join(WorkPath+'/','*_Hog.npy')))
#    SelMetaCurr = []
#    SelFeaCurr = []
#    #for dd in range(len(DayList)):
#    for dd in range(1, len(DayList), 4):
#    #for dd in range(5,10):
#        logging.warning('Day' + str(dd))
#        SelMetaCurr.append(pd.read_csv(DayList[dd], delimiter='\t'))
#        SelFeaCurr.append(np.load(DayListFea[dd]))
#        
#    SelMetaTot = pd.concat(SelMetaCurr) # merge dataframe
#    del SelMetaCurr
#    SelMetaTot = SelMetaTot.reset_index() # reset index
#
#    SelFeaTot = np.vstack(SelFeaCurr)
#    del SelFeaCurr

