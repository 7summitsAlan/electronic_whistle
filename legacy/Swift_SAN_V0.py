# -*- coding: utf-8 -*-
"""
Created on Wed Apr 05 09:32:55 2017

New Zealand

To-do-jobs
1. long-term spectrogram or cqt-based spectrogram (freq vs day)
2. power-law-based onset events (hourly count vs day)
3. freq-band events (frer-band count vs time)

Steps:
1. Check the time from the filenames for all the units.
2. Find the common/ overlapped duration
3. draw individual STFT <<== start with this

Time: 
1. focus the dates between 20160210 - 20160219
2. 10 days!
3. UTC time: New Zealand time is UTC/GMT +12 hours for stanard time and +13 for daylight saving time
4. Time is not precise. Neet to find a way to store the FFT results

Challenges:
1. Need to find a way of selecting the expected days and time for the analysis and ignoring others

    
@author: ys587
"""

FLAG_FIG = True
#FLAG_DEBUG = True
TEST_SNGLE_FILE = False

import os, glob
import time
import sys
import soundfile as sfile
import librosa as rosa
import matplotlib.pyplot as plt
import numpy as np
import re
#from multiprocessing import Pool

FreqReso = 10 # 10 Hz per point
# Hop length affects the time resolution
TimeReso = 30 # For each 10 sec , we have a one point
FsRef  = 48000
NumFFT = FsRef/FreqReso
NumFFTHalf = NumFFT/2 + 1

#regex = re.compile("_(\d{8})$") # YYYYMMDD
regex = re.compile("_(\d{4}-\d{2}-\d{2})$") # YYYYMMDD

def DaySpectroCalc(DayName):
    Samples, Fs = sfile.read(DayName)
    Day_Spectrogram = rosa.stft(Samples, n_fft = NumFFT, hop_length = Fs) 
    return Day_Spectrogram

def DaySpectroCalcSimple(DayName, TimeReso):
    Samples, Fs = sfile.read(DayName)
    Day_Spectrogram0 = abs(rosa.stft(Samples, n_fft = NumFFT, hop_length = Fs))
    Day_Spectrogram_DimT = int(np.floor(Day_Spectrogram0.shape[1]/TimeReso))
    Day_Spectrogram = np.zeros([Day_Spectrogram0.shape[0], Day_Spectrogram_DimT])
    for ii in range(Day_Spectrogram_DimT):
        Day_Spectrogram[:,ii] = Day_Spectrogram0[:, ii*TimeReso:(ii+1)*TimeReso].mean(axis=1)
    return Day_Spectrogram
        
if __name__ == "__main__":
    SitePathBase = r'G:\4849721\SWIFT_000_Normal'
    VisOutputPath = r'N:\users\yu_shiu_ys587\__Soundscape\__SAN'
    
    SiteNameList = os.listdir(SitePathBase)
    
    for SiteName in SiteNameList:
    #for SiteName in SiteNameList[:2]: #############################<<<<<<<<<<<<<<<<<<<<<<<<<<===================================
        #SitePath = r'C:\ASE_Data\__NewZealand\S1068NZ01_S01'
        ##SitePath = r'F:\S1068_NZ01_201612_UniformDays\S1068NZ01_S01'
        #SitePath = r'C:\ASE_Data\__NewZealand\S1068NZ01_S01\S1068NZ01_S01_20161211'
        #SitePath = SitePath.replace('\\','/')
        #SiteName = os.path.split(SitePath)[-1]
        SitePath = os.path.join(SitePathBase, SiteName)
        
        if FLAG_FIG:
            #fig, axarr = plt.subplots(3, 3, sharex=True, sharey=True)
            fig, axarr = plt.subplots(4, 3, sharex=True, sharey=True, figsize=(24.0, 9.0)) ###<<<===
            fig.text(0.5, 0.04, 'Time (Hour)', ha='center')
            fig.text(0.04, 0.5, 'Frequency (Hz)', va='center', rotation='vertical')
        
        WorkList = sorted(glob.glob(os.path.join(SitePath+'/','*'))) 
        cc = 0
        for dd in range(len(WorkList)): # days in a single site
        #for dd in range(2,11):
        #for dd in range(2): #############################<<<<<<<<<<<<<<<<<<<<<<<<<<===================================
            WorkList[dd] = WorkList[dd].replace('\\','/')
            print WorkList[dd]
            DayList = sorted(glob.glob(os.path.join(WorkList[dd]+'/','*')))
            
            m = regex.search(os.path.split(WorkList[dd])[-1])
            NameYYYYMMDD = m.groups()[0]
        
            #t1 = time.time()
            #DaySpectrogramAbs = abs(DaySpectroCalc(DayList[0]))
            
            print "Calculating spectrogram..."
            DaySpectrogramAbs = []
            #for ii in range(2): #############################<<<<<<<<<<<<<<<<<<<<<<<<<<===================================
            for ii in range(len(DayList)): # sound files in a single day
                #if ii % 10 == 0:
                print "Sound file from " + str(ii) + "..."
                #DaySpectrogramAbs.append(DaySpectroCalc(DayList[ii]))
                DaySpectrogramAbs.append(DaySpectroCalcSimple(DayList[ii], TimeReso))
                    
            print "Merging..."
            DaySpectrogramAbsTot = np.hstack(DaySpectrogramAbs)
            
            if FLAG_FIG:
                #axarr[dd/2, dd%2].imshow(DaySpectrogramAbsTot**.1, origin='lower', aspect='equal')
                Fs = sfile.info(DayList[dd]).samplerate
                XYLim = [0.0, 24.0, 0.0, FsRef/2.0]
                axarr[cc/3, cc%3].imshow(DaySpectrogramAbsTot**.1, origin='lower', extent=XYLim, aspect='auto') #############################<<<<<<<<<<<<<<<<<<<<<<<<<<===================================
                axarr[cc/3, cc%3].set_title(NameYYYYMMDD)
                cc += 1
    
        #plt.show()

        #fig.savefig(os.path.split(SitePath)[-1]+".png") ##<<=== need add time
        #fig.savefig(SiteName+".png")
        fig.savefig(os.path.join(VisOutputPath, SiteName+".png"))
