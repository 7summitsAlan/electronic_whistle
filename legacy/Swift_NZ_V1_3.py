# -*- coding: utf-8 -*-
"""
Created on Wed Apr 05 09:32:55 2017

New Zealand Long-Term Spectrogram (CQT) for every site and day
# V1_3: DRAWING ==>> draw figures for each day of multi-sites instead of each site of multi-days
# UNDER CONSTRUCTION => not yet started coding

# V1_2: CALCULATING ==>> calculate the CQT and save them; drawing will be handled by V1_3
# Assumption: 
(1) all sites have identical range of days; 
(2) the list of days will come from site 1; 
(3) we'll deal with non-identical range of days later but sorting the filenames

# V1_1: move most works into a function, in order to release memory
# Memory issue: out of memory after 2 site figures V1

To-do-jobs
1. long-term spectrogram or cqt-based spectrogram (freq vs day)
2. power-law-based onset events (hourly count vs day)
3. freq-band events (frer-band count vs time)

Steps:
1. Check the time from the filenames for all the units.
2. Find the common/ overlapped duration
3. draw individual STFT <<== start with this

Time: 
1. focus the dates between 20160210 - 20160219
2. 10 days!
3. UTC time: New Zealand time is UTC/GMT +12 hours for stanard time and +13 for daylight saving time
4. Time is not precise. Neet to find a way to store the FFT results

Challenges:
1. Need to find a way of selecting the expected days and time for the analysis and ignoring others

    
@author: ys587
"""

FLAG_FIG = False
#FLAG_DEBUG = True
TEST_SNGLE_FILE = False

import os, glob
import time
import sys
import soundfile as sfile
import librosa as rosa
import matplotlib.pyplot as plt
import numpy as np
import re
#from multiprocessing import Pool

# For STFT
FreqReso = 10 # 10 Hz per point
# Hop length affects the time resolution
FsRef  = 32000
NumFFT = FsRef/FreqReso
NumFFTHalf = NumFFT/2 + 1

# For CQT
hop_length = 512
TimeScaleSec = 30. # 30 sec
FMin = 30
NumOct = 9
MidiPerOct = 12

#FMin = 120
#NumOct = 7
#MidiPerOct = 36

regex = re.compile("_(\d{8})$") # YYYYMMDD

def DaySpectroCalcCQTSimple(DayName, TimeScaleSec=1):
    TimeScale = int(TimeScaleSec/(hop_length/float(FsRef))) 
    
    Samples, Fs = sfile.read(DayName)
    #Day_Spectrogram0 = abs(rosa.stft(Samples, n_fft = NumFFT, hop_length = Fs))
    Day_Spectrogram0 = abs(rosa.cqt(Samples, sr=Fs, hop_length=512, fmin=FMin, n_bins=NumOct*MidiPerOct, bins_per_octave=MidiPerOct))
    #Day_Spectrogram0 = abs(rosa.cqt(Samples, sr=Fs, hop_length=512, fmin=15, n_bins=10*6, bins_per_octave=6))
    #Day_Spectrogram0 = abs(rosa.cqt(Samples, sr=Fs, hop_length=512, fmin=120, n_bins=5*6, bins_per_octave=6))
    
    Day_Spectrogram_DimT = int(np.floor(Day_Spectrogram0.shape[1]/TimeScale))
    Day_Spectrogram = np.zeros([Day_Spectrogram0.shape[0], Day_Spectrogram_DimT])
    for ii in range(Day_Spectrogram_DimT):
        Day_Spectrogram[:,ii] = Day_Spectrogram0[:, ii*TimeScale:(ii+1)*TimeScale].mean(axis=1)
    return Day_Spectrogram

def OneSiteDayCQT(TargetDay, TimeScaleSec):    
    FileList = sorted(glob.glob(os.path.join(TargetDay+'/','*')))
    
    print "Calculating spectrogram..."
    DaySpectrogramAbs = []
    #for ii in range(len(FileList)): # sound files in a single day
    for ii in range(2): #############################<<<<<<<<<<<<<<<<<<<<<<<<<<===================================
        if ii % 1 == 0:                    
            print "Sound file from " + str(ii) + "..."
        DaySpectrogramAbs.append(DaySpectroCalcCQTSimple(FileList[ii], TimeScaleSec))
                        
    print "Merging..."
    DaySpectrogramAbsTot = np.hstack(DaySpectrogramAbs)
    del DaySpectrogramAbs
    print "DaySpectrogramAbsTot shape: " + str(DaySpectrogramAbsTot.shape)
    
    return DaySpectrogramAbsTot
                    
def OneDayMultiSiteSpectro(TargetSite):
    
    
    
    DayList = sorted(glob.glob(os.path.join(TargetSite+'/','*')))
    
    print "Calculating spectrogram..."
    DaySpectrogramAbs = []
    for ii in range(len(DayList)): # sound files in a single day
    #for ii in range(2): #############################<<<<<<<<<<<<<<<<<<<<<<<<<<===================================
        if ii % 1 == 0:                    
            print "Sound file from " + str(ii) + "..."
        DaySpectrogramAbs.append(DaySpectroCalcCQTSimple(DayList[ii], TimeScaleSec))
                        
    print "Merging..."
    DaySpectrogramAbsTot = np.hstack(DaySpectrogramAbs)
    del DaySpectrogramAbs
    print "DaySpectrogramAbsTot shape: " + str(DaySpectrogramAbsTot.shape)
    
    return DaySpectrogramAbsTot

#def OneSiteMultiDaySpectro(TargetDay, TimeScaleSec ):
#    DayList = sorted(glob.glob(os.path.join(TargetDay+'/','*')))
#    
#    print "Calculating spectrogram..."
#    DaySpectrogramAbs = []
#    for ii in range(len(DayList)): # sound files in a single day
#    #for ii in range(2): #############################<<<<<<<<<<<<<<<<<<<<<<<<<<===================================
#        if ii % 1 == 0:                    
#            print "Sound file from " + str(ii) + "..."
#        DaySpectrogramAbs.append(DaySpectroCalcCQTSimple(DayList[ii], TimeScaleSec))
#                        
#    print "Merging..."
#    DaySpectrogramAbsTot = np.hstack(DaySpectrogramAbs)
#    del DaySpectrogramAbs
#    print "DaySpectrogramAbsTot shape: " + str(DaySpectrogramAbsTot.shape)
#    
#    return DaySpectrogramAbsTot

                
if __name__ == "__main__":
    SitePathBase = r'F:\S1068_NZ01_201612_UniformDays'
    VisOutputPath = r'N:\users\yu_shiu_ys587\__SoundScape\__NewZealand\__CQTData'
    
    DeployName = 'S1068NZ01'
    # Site Format SXX. E.g. S05, S10
    DayList = ['20161210', '20161211', '20161212', '20161213', '20161214', '20161215', '20161216', '20161217', '20161218']
    NumOfSite = 10
    
    # Basename: SiteNumber + Date
    # e.g. S02_20161213 or S10_20161216

    # matrix holding all the day-site folder names
    # 2D list: NumOfSite xNumOfDay
    SiteDayList = [[None for x in range(len(DayList))] for y in range(NumOfSite)]
    for ss in range(NumOfSite):
        if ss+1 != NumOfSite:
            SiteDayList[ss] = sorted(glob.glob(os.path.join(SitePathBase, DeployName+'_S0'+str(ss+1), '*')))
        else: #SiteNum==10
            SiteDayList[ss] = sorted(glob.glob(os.path.join(SitePathBase, DeployName+'_S'+str(ss+1), '*')))
    
    ## Compute STFT/CQT for each day-site folder and save them through pickle    
    #for dd in range(len(DayList)):
    for dd in range(3):
        print 'dd: ' + str(dd)
        #for ss in range(NumOfSite):
        for ss in range(4):
            print 'ss: '+str(ss)
            Day_Spectro = []
            print SiteDayList[ss][dd]
            Day_Spectro = OneSiteDayCQT(SiteDayList[ss][dd], TimeScaleSec)
            np.save(os.path.join(VisOutputPath, DeployName+'_'+str(ss+1)+'_'+DayList[dd]), Day_Spectro)
            
    ## Read from the npy files & draw figures
    # example: Day_Spectro0 = np.load(os.path.join(VisOutputPath, DeployName+'_'+str(ss+1)+'_'+DayList[dd]+'.npy'))
    # do/draw what? 
    # Pick a day and draw over all sites
    ddTarget = 2 # DayTarget = DayList[ddTarget]
    
    if FLAG_FIG:
        #fig, axarr = plt.subplots(3, 3, sharex=True, sharey=True)
        fig, axarr = plt.subplots(nrows=4, ncols=3, sharex=True, sharey=True, figsize=(24.0, 9.0)) 
        fig.text(0.5, 0.04, 'Time (Hour)', ha='center')
        fig.text(0.04, 0.5, 'Frequency (Hz)', va='center', rotation='vertical')

        DaySpectrogramAbsTot = []
        DaySpectrogramAbsTot = OneSiteMultiDaySpectro(WorkList[dd])
    
        for ss in range(NumOfSite):
            Day_Spectro0 = np.load(os.path.join(VisOutputPath, DeployName+'_'+str(ss+1)+'_'+DayList[ddTarget]+'.npy'))
            DimT = DaySpectrogramAbsTot.shape[1]
            TimeScale = int(TimeScaleSec/(hop_length/float(FsRef)))
            xx = np.arange(0, DimT)*TimeScale*512./FsRef/3600
            yy = FMin*2**(np.arange(0, (NumOct*1.0)*MidiPerOct)/MidiPerOct)
            axarr[cc/3, cc%3].pcolor(xx, yy, (DaySpectrogramAbsTot)**.1)
            axarr[cc/3, cc%3].set_yscale('log')
            axarr[cc/3, cc%3].set_title(NameYYYYMMDD)
            cc += 1            
        

            

#for SiteName in SiteNameList[2:]:
#    SitePath = os.path.join(SitePathBase, SiteName)
#    
#    if FLAG_FIG:
#        #fig, axarr = plt.subplots(3, 3, sharex=True, sharey=True)
#        fig, axarr = plt.subplots(nrows=3, ncols=3, sharex=True, sharey=True, figsize=(18.0, 9.0)) 
#        fig.text(0.5, 0.04, 'Time (Hour)', ha='center')
#        fig.text(0.04, 0.5, 'Frequency (Hz)', va='center', rotation='vertical')
#    
#    WorkList = sorted(glob.glob(os.path.join(SitePath+'/','*'))) 
#    cc = 0
#    for dd in range(len(WorkList)): # days in a single site
#    #for dd in range(2): #############################<<<<<<<<<<<<<<<<<<<<<<<<<<===================================
#        WorkList[dd] = WorkList[dd].replace('\\','/')
#        print WorkList[dd]
#        m = regex.search(os.path.split(WorkList[dd])[-1])
#        NameYYYYMMDD = m.groups()[0]
#        
#        DaySpectrogramAbsTot = []
#        DaySpectrogramAbsTot = OneSiteMultiDaySpectro(WorkList[dd])
#            
#        if FLAG_FIG:
#            #Fs = sfile.info(DayList[dd]).samplerate
#            DimT = DaySpectrogramAbsTot.shape[1]
#            xx = np.arange(0, DimT)*TimeScale*512./FsRef/3600
#            yy = FMin*2**(np.arange(0, (NumOct*1.0)*MidiPerOct)/MidiPerOct)
#            axarr[cc/3, cc%3].pcolor(xx, yy, (DaySpectrogramAbsTot)**.1)
#            axarr[cc/3, cc%3].set_yscale('log')
#            axarr[cc/3, cc%3].set_title(NameYYYYMMDD)
#            cc += 1
#
#    fig.savefig(os.path.join(VisOutputPath, SiteName+".png"))
#    
#    #plt.close(fig)
#    del fig, axarr
#                 
        
        
            
#SiteNameList = os.listdir(SitePathBase)
    #SitePath = os.path.join(SitePathBase, SiteNameList[0]) # Use the first site to list all days
    #DayList = sorted(glob.glob(os.path.join(SitePath+'/','*'))) 


#    for Day in DayList:
#        if FLAG_FIG:
#            fig, axarr = plt.subplots(nrows=4, ncols=3, sharex=True, sharey=True, figsize=(18.0, 9.0)) 
#            fig.text(0.5, 0.04, 'Time (Hour)', ha='center')
#            fig.text(0.04, 0.5, 'Frequency (Hz)', va='center', rotation='vertical')

        


    
#    for SiteName in SiteNameList:
#    #for SiteName in SiteNameList[2:4]: #############################<<<<<<<<<<<<<<<<<<<<<<<<<<===================================
#        SitePath = os.path.join(SitePathBase, SiteName)
#        
#        if FLAG_FIG:
#            #fig, axarr = plt.subplots(3, 3, sharex=True, sharey=True)
#            fig, axarr = plt.subplots(nrows=3, ncols=3, sharex=True, sharey=True, figsize=(18.0, 9.0)) 
#            fig.text(0.5, 0.04, 'Time (Hour)', ha='center')
#            fig.text(0.04, 0.5, 'Frequency (Hz)', va='center', rotation='vertical')
#        
#        WorkList = sorted(glob.glob(os.path.join(SitePath+'/','*'))) 
#        cc = 0
#        for dd in range(len(WorkList)): # days in a single site
#        #for dd in range(2): #############################<<<<<<<<<<<<<<<<<<<<<<<<<<===================================
#            WorkList[dd] = WorkList[dd].replace('\\','/')
#            print WorkList[dd]
#            
#            DaySpectrogramAbsTot = []
#            DaySpectrogramAbsTot = OneSiteMultiDaySpectro(WorkList[dd])
#            m = regex.search(os.path.split(WorkList[dd])[-1])
#            NameYYYYMMDD = m.groups()[0]
#                
#            if FLAG_FIG:
#                #Fs = sfile.info(DayList[dd]).samplerate
#                DimT = DaySpectrogramAbsTot.shape[1]
#                xx = np.arange(0, DimT)*TimeScale*512./FsRef/3600
#                yy = FMin*2**(np.arange(0, (NumOct*1.0)*MidiPerOct)/MidiPerOct)
#                axarr[cc/3, cc%3].pcolor(xx, yy, (DaySpectrogramAbsTot)**.1)
#                axarr[cc/3, cc%3].set_yscale('log')
#                axarr[cc/3, cc%3].set_title(NameYYYYMMDD)
#                cc += 1
#    
#        fig.savefig(os.path.join(VisOutputPath, SiteName+".png"))
#        
#        #plt.close(fig)
#        del fig
                     
        
        
        
        
if False:
    fig, axarr = plt.subplots(2, 2, sharex=True, figsize=(80, 60))
    axarr[0, 0].imshow(DaySpectrogramAbsTot**.1, origin='lower', aspect='equal')
    plt.show()

if False:
    DayList = sorted(glob.glob(os.path.join(WorkList[dd]+'/','*')))
    
    m = regex.search(os.path.split(WorkList[dd])[-1])
    NameYYYYMMDD = m.groups()[0]

    #t1 = time.time()
    #DaySpectrogramAbs = abs(DaySpectroCalc(DayList[0]))
    
    print "Calculating spectrogram..."
    DaySpectrogramAbs = []
    for ii in range(len(DayList)): # sound files in a single day
    #for ii in range(2): #############################<<<<<<<<<<<<<<<<<<<<<<<<<<===================================
        if ii % 1 == 0:                    
            print "Sound file from " + str(ii) + "..."
        DaySpectrogramAbs.append(DaySpectroCalcCQTSimple(DayList[ii], TimeScale))
            
    print "Merging..."
    DaySpectrogramAbsTot = np.hstack(DaySpectrogramAbs)
    del DaySpectrogramAbs
    print "DaySpectrogramAbsTot shape: " + str(DaySpectrogramAbsTot.shape)
    #print "DaySpectrogramAbsTot T shape: " + str((DaySpectrogramAbsTot.T).shape)
    