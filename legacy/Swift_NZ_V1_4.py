# -*- coding: utf-8 -*-
"""
Created on Mon Apr 17 19:32:40 2017

Swift Analysis

V1_4: powerlaw energy detection
Feature: power disttribution over frequencies

@author: ys587

Soundscape by detection

TO-DO:
* Dominant freq or dominant fundamental freq (by correlating a harmonic filter on const-Q domain)
* More dimensions: MARU location, freq, MFCC, and etc
"""
import os
import glob
import re
import datetime
import soundfile as sf
import matplotlib.pyplot as plt
import numpy as np
import librosa
import scipy.signal as signal
import time
from multiprocessing import Pool #Process, Queue,
#import parmap
import itertools

TIME_FORMAT = "%Y%m%d_%H%M%S" # this is how your timestamp looks like
regex = re.compile("_(\d{8}_\d{6})Z") # YYYYMMDD_HHMMSS
#regex1 = re.compile("_(\d{2})(\d{2})(\d{2})$") # YYYYMMDD_HHMMSS
#regex1 = re.compile("_(\d{2})(\d{2})(\d{2})_") # YYYYMMDD_HHMMSS

# Sound path & selection table output path
#WorkPath = r'N:\projects\2006_Excelerate_MassBay_52965\52965_Dep20_20120328\52965_Dep20_AIFF'
#SelTabPath = r'N:\users\yu_shiu_ys587\__MassBay\__Dep20_2012\__LogHarm'

# Debug flags
FLAG_PLOT = 0
FLAG_PARALLEL = False

# Parameters
Nu1 = 2.0
Nu2 = 1.0
#Gamma = 1.0
Gamma = 1.0

SegLen = 60 # segment length for Power Law, 60 sec
SampleRateRef = 32000

#TDetecThre = 0.15 # powerlaw value
#TDetecThre = 1.2
TDetecThre = 0.5

## CQT  ##
#FreqMin = 30
#BinsPerOctave = 12
#OctaveNum = 9

HopLength = 2**12
EventTimeReso = HopLength*1.0/SampleRateRef

#TDetecThreLen = 0.25 # in sec
TDetecThreLen = 0.05 # in sec
#TDetecThreLen = max([EventTimeReso*3.0, TDetecThreLen])

FreqMin = 200
BinsPerOctave = 6
OctaveNum = 6

## STFT instead of CQT ##
#NumFFT = 3200
#EventTimeReso = 3200./4/SampleRateRef

BinTotal = BinsPerOctave*OctaveNum

Winn = np.hamming(21)
Winn = Winn/np.sum(Winn)

def GetTimeStamp(TheString):
    m = regex.search(TheString)
    return datetime.datetime.strptime(m.groups()[0], TIME_FORMAT)

def SoundRead(FileName):
    Samples, SampleRate = sf.read(FileName)
    #return Samples, SampleRate, NumChan, NumFrame
    return Samples, SampleRate

"""
def STFT_Yu(Samples0, WinSize, FFTSize, HopSize):
    TotalSegments = np.int32(np.ceil(len(Samples0) / np.float32(HopSize)))
    Window = np.hanning(WinSize)
    Proc = np.concatenate((Samples0, np.zeros(WinSize))) # the data to process
    Spectrogram = np.empty((TotalSegments, int(0.5*FFTSize)+1), dtype=float)
    for i in xrange(TotalSegments): # for each segment
        CurrentHop = HopSize * i  # figure out the current segment offset
        Segment = Proc[CurrentHop: CurrentHop+WinSize]  # get the current segment
        Windowed = Segment * Window  # multiply by the half cosine function
        Padded = np.append(Windowed, np.zeros(FFTSize-WinSize))  # add 0s to double the length of the data
        Spectrum = np.fft.fft(Padded)
        Autopower = np.abs(Spectrum * np.conj(Spectrum))  # find the autopower spectrum
        Spectrogram[i, :] = Autopower[0:int(0.5*FFTSize)+1]
    return Spectrogram
"""

def PowerLawMatCal(SpectroMat, Nu1, Nu2, Gamma):
    DimF, DimT = SpectroMat.shape
    Mu_k = [PoweLawFindMu(SpectroMat[ff,:]) for ff in range(SpectroMat.shape[0])]
    
    Mat0 = SpectroMat**Gamma - np.array(Mu_k).reshape(DimF,1)*np.ones((1, DimT))
    MatADenom = [(np.sum(Mat0[:,tt]**2.))**.5 for tt in range(DimT)]
    MatA = Mat0 / (np.ones((DimF,1)) * np.array(MatADenom).reshape(1, DimT) )
    MatBDenom = [ (np.sum(Mat0[ff,:]**2.))**.5 for ff in range(DimF)]
    MatB = Mat0 / (np.array(MatBDenom).reshape(DimF,1) * np.ones((1, DimT)))
    #PowerLawTFunc = np.sum((MatA**Nu1)*(MatB**Nu2), axis=0)
    PowerLawMat = (MatA**Nu1)*(MatB**Nu2)
    #return PowerLawTFunc
    return PowerLawMat
    
def PoweLawFindMu(SpecTarget):
    SpecSorted = np.sort(SpecTarget)
    SpecHalfLen = int(np.floor(SpecSorted.shape[0]*.5))
    IndJ = np.argmin(SpecSorted[SpecHalfLen:SpecHalfLen*2] - SpecSorted[0:SpecHalfLen])
    Mu = np.mean(SpecSorted[IndJ:IndJ+SpecHalfLen])
    return Mu

def FindIndexNonZero(DiffIndexSeq):
    IndSeq = []
    ItemSeq = []
    for index, item in enumerate(DiffIndexSeq):
        if(item != 0):
            IndSeq.append(index)
            ItemSeq.append(item)
    return IndSeq, ItemSeq

# for using two or more arguments
def universal_worker(input_pair):
    function, args = input_pair
    return function(*args)

def pool_args(function, *args):
    return zip(itertools.repeat(function), zip(*args))

def DetectViaPowerLaw(DaySound, SelTabPath):
    DaySound = DaySound.replace('\\','/')
    SoundList = sorted(glob.glob(os.path.join(DaySound+'/', '*.flac')))
    
    SelTabPath = SelTabPath.replace('\\','/')
    #DayFile = os.path.join(SelTabPath+'/',os.path.basename(DaySound)+'.txt')
    #DayFile = os.path.join(SelTabPath+'/',os.path.splitext(os.path.basename(DaySound))[0]+'.txt')
    DayFile = os.path.join(SelTabPath+'/',os.path.basename(DaySound)+'.txt')
    
    #if not os.path.exists(DayFile):
    if True:
        f = open(DayFile,'w')
        #f.write('Selection\tView\tChannel\tBegin Time (s)\tEnd Time (s)\tLow Freq (Hz)\tHigh Freq (Hz)\tScore\n')
        f.write('Selection\tView\tChannel\tBegin Time (s)\tEnd Time (s)\tLow Freq (Hz)\tHigh Freq (Hz)\tScore')
        #for hh in range(BinTotal):
        #for hh in range(BinTotal*2):
        for hh in range(BinTotal):
            f.write('\tFea'+str(hh))
        f.write('\n')
        EventId=0
        
        #if True:        
        for ff in SoundList:
        #for ff in SoundList[72:96]:
            #ff = DaySound
            ff2 = os.path.splitext(os.path.basename(ff))[0]
            if not FLAG_PARALLEL:
                print ff2                    
            # time stamp
            TimeCurr = GetTimeStamp(ff2)
            if not FLAG_PARALLEL:
                print TimeCurr
                print TimeCurr.hour, TimeCurr.minute, TimeCurr.second
               
            # read sound file
            Samples0, SampleRate0 = SoundRead(ff)
            # resampling 
            if (SampleRate0 != SampleRateRef):
                Samples0 = signal.resample(Samples0, float(len(Samples0))/SampleRate0*SampleRateRef)
            
            # make single-channel sound multi-dimensional array
            if(Samples0.ndim == 1):
                Samples0 = np.array([Samples0]).T
            
            for cc in range(Samples0.shape[1]):            
            #for cc in range(2, 5): # channel number
                if not FLAG_PARALLEL:
                    print "Channel "+str(cc)
                
                for ss in range(int(np.floor(Samples0.shape[0]*1.0/SampleRateRef/SegLen))): # fixed the problem on SampleRate. Use SampleRate0 mistakenly at first.
                #for ss in range(12):
                    Samples = Samples0[ss*SegLen*SampleRateRef:(ss+1)*SegLen*SampleRateRef,cc]
                    Samples = Samples - Samples.mean()
                    
                    # Constant-Q
                    #BinsPerOctave = 12*2
                    CqtMat = abs(librosa.cqt(Samples, hop_length=HopLength, bins_per_octave=BinsPerOctave, n_bins=BinTotal, sr=SampleRateRef, fmin=FreqMin))
                    #CqtMat = abs(librosa.stft(Samples, n_fft = NumFFT))
                    
                    #np.iscomplex((librosa.cqt(Samples, hop_length=HopLength, bins_per_octave=BinsPerOctave, n_bins=BinTotal, sr=SampleRateRef, fmin=FreqMin)).flatten()).sum()
                    
                    #if (np.iscomplex(CqtMat.flatten())).sum():
                    #    print 'Complex!'
                    PowerLawCqt = PowerLawMatCal(CqtMat, Nu1, Nu2, Gamma)
                    PowerLawCqt = PowerLawCqt*(PowerLawCqt>0.0) # make sure PowerLawCqt0 is positive and so is its mean for each freq
                    PowerLawCqtMu = np.mean(PowerLawCqt, axis=1)
                    #PowerLawCqtMu = signal.medfilt(PowerLawCqtMu, 3)
                    PowerLawCqtMuMat = np.array([PowerLawCqtMu]).T *np.ones([1, PowerLawCqt.shape[1]])
                    PowerLawCqt = PowerLawCqt - PowerLawCqtMuMat
                    PowerLawCqt = PowerLawCqt*(PowerLawCqt>=0)
                    PowerLawCqt = PowerLawCqt / PowerLawCqtMuMat
                                        
                    #TDetecFunc0 = (np.sum(PowerLawCqt, axis=0))**2.0
                    #TDetecFunc0 = np.sum(PowerLawCqt, axis=0)/BinTotal
                    TDetecFunc0 = np.mean(PowerLawCqt, axis=0)
                    TDetecFunc = np.convolve(TDetecFunc0, Winn, mode='same')
                    #TDetecFunc = signal.medfilt(TDetecFunc0, 21) # <<=== median filter!
                    #TDetecFunc = np.convolve(TDetecFunc, Winn, mode='same')
                    
                    TDetecRange = (TDetecFunc > TDetecThre).astype(int)
                    TDetecRangeDiff = np.diff(TDetecRange)
                    IndDiff, ValDiff = FindIndexNonZero(TDetecRangeDiff)
                                        
                    EventList = []
                    if ValDiff: # test if not empty
                        if(ValDiff[0] == -1):
                            ValDiff = [1] + ValDiff
                            IndDiff = [0] + IndDiff
                        if(ValDiff[-1] == 1):
                            ValDiff.append(-1)
                            IndDiff.append(len(TDetecRangeDiff))
                        for tt in range(0, len(ValDiff), 2):
                            #TimeE1 = IndDiff[tt]*EventTimeReso
                            #TimeE2 = (IndDiff[tt+1]+1)*EventTimeReso
                            TimeE1 = IndDiff[tt]
                            TimeE2 = (IndDiff[tt+1]+1)
                            EventTVal = np.max(TDetecFunc[IndDiff[tt]:(IndDiff[tt+1]+1)]) # This is the score! It is the max value in the detection function.
                            
                            # FEATURE: FreqHist
                            FreqHist0 = np.mean(PowerLawCqt[:,TimeE1:TimeE2], axis=1)
                            
                            FreqHist = FreqHist0 - FreqHist0.mean()
                            FreqHistCumSum = np.cumsum(FreqHist0)
                            EneLow = np.percentile(FreqHistCumSum, 40)
                            EneHigh = np.percentile(FreqHistCumSum, 85)
                            F1 = FreqHist0[FreqHistCumSum < EneLow].shape[0]-1
                            F2 = FreqHist0[FreqHistCumSum < EneHigh].shape[0]
                            # CQT
                            Freq1 = FreqMin*(2.**(float(F1)/BinsPerOctave))
                            Freq2 = FreqMin*(2.**(float(F2)/BinsPerOctave))
                            
                            FreqHist = FreqHist0*(FreqHist0>0)
                            FreqHist = FreqHist/np.sum(FreqHist)
                            
                            if(TDetecThreLen <= (TimeE2-TimeE1)*EventTimeReso):
                                #EventList.append([TimeE1,TimeE2,EventTVal, FreqHist])
                                EventList.append([TimeE1,TimeE2,EventTVal, Freq1, Freq2, FreqHist])                            
                            
                        # write to selection table
                        for ee in range(len(EventList)):
                            EventId += 1
                            Time1 = TimeCurr.hour*3600. + TimeCurr.minute*60.0 + TimeCurr.second + ss*SegLen*1.0 + EventList[ee][0]*EventTimeReso
                            Time2 = TimeCurr.hour*3600. + TimeCurr.minute*60.0 + TimeCurr.second + ss*SegLen*1.0 + EventList[ee][1]*EventTimeReso
                            #f.write(str(EventId)+'\t'+'Spectrogram'+'\t'+str(cc+1)+'\t'+str.format("{0:=.4f}",Time1)+'\t'+str.format("{0:<.4f}",Time2)+'\t50.0\t350.0\t'+str.format("{0:<.4f}",EventList[ee][2]))
                            #f.write(str(EventId)+'\t'+'Spectrogram'+'\t'+str(cc+1)+'\t'+str.format("{0:=.4f}",Time1)+'\t'+str.format("{0:<.4f}",Time2)+'\t50\t350\t'+str.format("{0:<.4f}",EventList[ee][2]))
                            f.write(str(EventId)+'\t'+'Spectrogram'+'\t'+str(cc+1)+'\t'+str.format("{0:=.4f}",Time1)+'\t'+str.format("{0:<.4f}",Time2)+'\t'+str.format("{0:=.4f}",EventList[ee][3])+'\t'+str.format("{0:=.4f}",EventList[ee][4])+'\t'+str.format("{0:<.4f}",EventList[ee][2]))
                            for hh in range(BinTotal):
                                #f.write('\t'+str.format("{0:.6f}",EventList[ee][3][hh]))
                                f.write('\t'+str.format("{0:.6f}",EventList[ee][5][hh]))
                            f.write('\n')
        f.close()
        return os.path.basename(DaySound)+' was processed.'
    else:
        return os.path.basename(DaySound)+' already exists.'
        
if __name__ == '__main__':
    #SitePathBase = r'F:\S1068_NZ01_201612_UniformDays'
    #SitePathBase = r'N:\users\yu_shiu_ys587\__Soundscape\__NewZealandData\S1068_NZ01_201612_UniformDays'
    SitePathBase = r'G:\S1068_NZ01_201612_UniformDays'
    SelTabPath = r'P:\users\yu_shiu_ys587\__Soundscape\__NewZealand\__OnsetSelTableTemp'
    
    DeployName = 'S1068NZ01'
    # Site Format SXX. E.g. S05, S10
    DayList = ['20161210', '20161211', '20161212', '20161213', '20161214', '20161215', '20161216', '20161217', '20161218']
    NumOfSite = 10
    
    # matrix holding all the day-site folder names
    # 2D list: NumOfSite xNumOfDay
    SiteDayList = [[None for x in range(len(DayList))] for y in range(NumOfSite)]
    for ss in range(NumOfSite):
        if ss+1 != NumOfSite:
            SiteDayList[ss] = sorted(glob.glob(os.path.join(SitePathBase, DeployName+'_S0'+str(ss+1), '*')))
        else: #SiteNum==10
            SiteDayList[ss] = sorted(glob.glob(os.path.join(SitePathBase, DeployName+'_S'+str(ss+1), '*')))
            
    SiteDayListFlat = [SiteDayList[ss][dd] for ss in range(NumOfSite) for dd in range(len(DayList))]
    SelTabPathList = [SelTabPath] * len(SiteDayListFlat)

    if not FLAG_PARALLEL:
        DetectViaPowerLaw(SiteDayListFlat[4], SelTabPathList[4])
    else:
        #MyPool = Pool(7)    
        MyPool = Pool(6)    
        try:
            for ReturnedStr in MyPool.imap_unordered(universal_worker, pool_args(DetectViaPowerLaw, SiteDayListFlat, SelTabPathList)):
                print ReturnedStr
            time.sleep(10)
        except KeyboardInterrupt:
            print "Caught KeyboardInterrupt, terminating workers"
            MyPool.terminate()
            MyPool.join()
        else:
            print "Work is finished. Quitting gracefully"
            MyPool.close()
            MyPool.join()



