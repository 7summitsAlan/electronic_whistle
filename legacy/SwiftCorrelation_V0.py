# -*- coding: utf-8 -*-
"""
# correlation between long-term spectrogram

# 30 sec a point
# Use 10-min as a bin

Created on Wed Aug 23 13:44:47 2017
@author: ys587
"""

import numpy as np
import os

#SpecPath = r'P:\users\yu_shiu_ys587\__Soundscape\__NewZealand\__CQTData'
#SpecPath = r'P:\users\yu_shiu_ys587\__Soundscape\__NewZealand\__SpecData'
SpecPath = r'P:\users\yu_shiu_ys587\__Soundscape\__NewZealand\__SpecData2'

if __name__ == "__main__":
    DeployName = 'S1068NZ01'
    # Site Format SXX. E.g. S05, S10
    DayList = ['20161210', '20161211', '20161212', '20161213', '20161214', '20161215', '20161216', '20161217', '20161218']
    NumSite = 10
    
    print os.path.join(SpecPath, 'S1068NZ01'+'_1_'+DayList[0]+'.npy')
    # np.load()
    
    CorrMatList = []
    for dd in DayList:
        CorrMat = np.zeros([NumSite, NumSite])
        for ss in range(NumSite):
            print 'dd: '+ dd +' ss: ' + str(ss+1)
            Spectro1 = np.load(os.path.join(SpecPath, 'S1068NZ01'+'_'+str(ss+1)+'_'+ dd +'.npy'))
            print Spectro1.shape
            for tt in range(ss+1,NumSite):
                Spectro2 = np.load(os.path.join(SpecPath, 'S1068NZ01'+'_'+str(tt+1)+'_'+ dd +'.npy'))
                print 'ss: ' + str(ss+1) + '; tt: '+str(tt+1) + '; shape: ',
                print Spectro1.shape,
                print Spectro2.shape

                
        CorrMat = CorrMat + CorrMat.T + np.identity(NumSite)
        CorrMatList.append(CorrMat)