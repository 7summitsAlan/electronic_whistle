#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""

Extract images for learning representation in tensorflow
3 datasets: (i) __CHAOZX (ii) __NewZealand (iii) __SSW

cropped images / data augmentation / scale?

output: (i) selection table; (ii) images or waveform?

Goals: 
1. display long-term dataset
2. display short-term acoustic activity such as seiemic airgun on 20130902
periods of airgun pulses: 4 sec, 1 min. 
period of "gunshot" pulses: 3 min

Created on Mon Jun 12 11:38:56 2017

@author: atoultaro
"""
import os
import glob
import re
import datetime
import soundfile as sf
import matplotlib.pyplot as plt
import numpy as np
import librosa
import scipy.signal as signal
import time
from multiprocessing import Pool #Process, Queue,
#import parmap
import itertools
from scipy.stats import entropy
#from scipy.ndimage import zoom
#from scipy.misc import imresize
#from scipy import ndimage
from math import pi as Pi
import logging

logging.basicConfig(filename=r'P:\users\yu_shiu_ys587\__Soundscape\__ASA_Boston\__CHAOZX\example.log', level=logging.INFO)

# Debug flags
FLAG_PLOT = 0


TIME_FORMAT = "%Y%m%d_%H%M%S" # this is how your timestamp looks like
regex = re.compile("_(\d{8}_\d{6})") # YYYYMMDD_HHMMSS
regex1 = re.compile("_(\d{2})(\d{2})(\d{2})$") # YYYYMMDD_HHMMSS
#regex1 = re.compile("_(\d{2})(\d{2})(\d{2})_") # YYYYMMDD_HHMMSS

# Sound path & selection table output path
#WorkPath = r'N:\projects\2006_Excelerate_MassBay_52965\52965_Dep20_20120328\52965_Dep20_AIFF'
#SelTabPath = r'N:\users\yu_shiu_ys587\__MassBay\__Dep20_2012\__LogHarm'

# Parameters
#Nu1 = 3.0
#Nu2 = 1.0
Nu1 = 1.0
Nu2 = 2.0
#Nu1 = 2.5
#Nu2 = 0.5
# (2.5, 0.5) in GPL Matlab code
Gamma = 1.0
#SegLen = 60 # segment length for Power Law, 60 sec
#SegLen = 90 # 90 sec; 1.5 min
#SegLen = 180 # 3 min
SegLen = 300 # 5 min
SampleRateRef = 2000
HopLength = 2**6 # step size
#TDetecThre = 5e-4
TDetecThre = 2e-4
#TDetecThre = 1e-4 
#TDetecThre = 5e-5 ######### <<<<<<<<<<<<< ============ minimum detection score!!
#TDetecThre = 1e-5

# Relationship between hozsize and Detection threshold!!
# The larger the hop size is, the powerful the detection threshold is. It means higher number of detected events.

TDetecSmooth = 9 # median filter window for smoothing TDetec function; window length = 9*.032=
#EventTimeReso = HopLength*1.0/SampleRateRef # step size in sec
#TDetecThreLen = 0.10 # in sec no different from 0.2 sec since the time resolution is 0.128 sec
#TDetecThreLen = 0.2 # in sec ######### <<<<<<<<<<<<< ============ minimum duration!!
TDetecThreLen = 0.40
#TDetecThreLen = 0.64 # in sec, Mar 25, 2016

# FFT
FFTSize = 512
WinSize = FFTSize
#HopSize = 256 # .128 sec
#HopSize = 100 # 50 msec
HopSize = 200 # 100 msec
EventTimeReso = HopSize/float(SampleRateRef) # step size in sec

TimeE_Dur = 2.0 # sec
TimeE_DurHalf = int(np.floor(TimeE_Dur/2/EventTimeReso)) # int


IndFreqLow = int(np.floor(50./SampleRateRef*FFTSize))
IndFreqHigh = int(np.ceil(800./SampleRateRef*FFTSize))

def GetTimeStamp(TheString):
    m = regex.search(TheString)
    return datetime.datetime.strptime(m.groups()[0], TIME_FORMAT)

def SoundRead(FileName):
    Samples, SampleRate = sf.read(FileName)
    #return Samples, SampleRate, NumChan, NumFrame
    return Samples, SampleRate

"""
def STFT_Yu(Samples0, WinSize, FFTSize, HopSize):
    TotalSegments = np.int32(np.ceil(len(Samples0) / np.float32(HopSize)))
    Window = np.hanning(WinSize)
    Proc = np.concatenate((Samples0, np.zeros(WinSize))) # the data to process
    Spectrogram = np.empty((TotalSegments, int(0.5*FFTSize)+1), dtype=float)
    for i in xrange(TotalSegments): # for each segment
        CurrentHop = HopSize * i  # figure out the current segment offset
        Segment = Proc[CurrentHop: CurrentHop+WinSize]  # get the current segment
        Windowed = Segment * Window  # multiply by the half cosine function
        Padded = np.append(Windowed, np.zeros(FFTSize-WinSize))  # add 0s to double the length of the data
        Spectrum = np.fft.fft(Padded)
        Autopower = np.abs(Spectrum * np.conj(Spectrum))  # find the autopower spectrum
        Spectrogram[i, :] = Autopower[0:int(0.5*FFTSize)+1]
    return Spectrogram
"""

def PowerLawMatCal(SpectroMat, Nu1, Nu2, Gamma):
    DimF, DimT = SpectroMat.shape
    #DimTHalf = int(np.floor(DimT/2.))
    Mu_k = [PoweLawFindMu(SpectroMat[ff,:]) for ff in range(DimF)]
    #Mu_k = np.zeros(DimF)
    #for ff in range(DimF):
    #    TempKKSort = np.sort(SpectroMat[ff,:])
    #    Indff = np.argmin(TempKKSort[DimTHalf:2*DimTHalf] - TempKKSort[0:DimTHalf])
    #    Mu_k[ff] = TempKKSort[Indff:Indff+DimTHalf].mean()
    
    Mat0 = SpectroMat**Gamma - np.array(Mu_k).reshape(DimF,1)*np.ones((1, DimT))
    MatADenom = [(np.sum(Mat0[:,tt]**2.))**.5 for tt in range(DimT)]
    MatA = Mat0 / (np.ones((DimF,1)) * np.array(MatADenom).reshape(1, DimT) )
    MatBDenom = [ (np.sum(Mat0[ff,:]**2.))**.5 for ff in range(DimF)]
    MatB = Mat0 / (np.array(MatBDenom).reshape(DimF,1) * np.ones((1, DimT)))
    #PowerLawTFunc = np.sum((MatA**Nu1)*(MatB**Nu2), axis=0)
    MatA = MatA*(MatA>0) # set negative values into zero
    MatB = MatB*(MatB>0)
    #PowerLawMat = (MatA**Nu1)*(MatB**Nu2)
    PowerLawMat = (MatA**(2.0*Nu1))*(MatB**(2.0*Nu2))
    return PowerLawMat
    
def PoweLawFindMu(SpecTarget):
    SpecSorted = np.sort(SpecTarget)
    SpecHalfLen = int(np.floor(SpecSorted.shape[0]*.5))
    IndJ = np.argmin(SpecSorted[SpecHalfLen:SpecHalfLen*2] - SpecSorted[0:SpecHalfLen])
    Mu = np.mean(SpecSorted[IndJ:IndJ+SpecHalfLen])
    return Mu

def FindIndexNonZero(DiffIndexSeq):
    IndSeq = []
    ItemSeq = []
    for index, item in enumerate(DiffIndexSeq):
        if(item != 0):
            IndSeq.append(index)
            ItemSeq.append(item)
    return IndSeq, ItemSeq

# for using two or more arguments
def universal_worker(input_pair):
    function, args = input_pair
    return function(*args)

def pool_args(function, *args):
    return zip(itertools.repeat(function), zip(*args))

#def NameWin2Ubuntu(FileNameWin):
#    return FileNameUbuntu
#
#def NameUbuntu2Win(FileNameUbuntu):
#    return FileNameWin

def DetectViaPowerLaw(DaySound, SelTabPath):
    DaySound = DaySound.replace('\\','/')
    #SoundList = sorted(glob.glob(os.path.join(DaySound+'/', '*.aif')))
    SoundListTemp = glob.glob(os.path.join(DaySound+'/', '*.aif'))
    if len(SoundListTemp) ==0:
        SoundListTemp = glob.glob(os.path.join(DaySound+'/', '*.wav'))
        if len(SoundListTemp) ==0:
            return "Therea are no supported sound formats in .wav or .aif."
    SoundList = sorted(SoundListTemp)     
    
    SelTabPath = SelTabPath.replace('\\','/')
    DayFile = os.path.join(SelTabPath+'/',os.path.basename(DaySound)+'.txt')
    
    ImageVecList = []    
    #if not os.path.exists(DayFile):
    if True:
        f = open(DayFile,'w')
        f.write('Selection\tView\tChannel\tBegin Time (s)\tEnd Time (s)\tLow Freq (Hz)\tHigh Freq (Hz)\tScore\tDuration\tBegin Path\tFile Offset (s)')
        f.write('\n')
        EventId=0
        
        #ffCount = 0
        for ff in SoundList:
        #for ff in SoundList[28:48:5]:
        #for ff in SoundList[10]:
            ff2 = os.path.splitext(os.path.basename(ff))[0]
            ##print ff2
            logging.info(ff2)
            
            #if(ffCount == 29 or ffCount == 30):
            #    logging.info('Stop!!')
            
            # time stamp
            TimeCurr = GetTimeStamp(ff2)
            ##print TimeCurr
            logging.info(str(TimeCurr.hour)+':'+str(TimeCurr.minute)+':'+str(TimeCurr.second))
            # read sound file
            Samples0, SampleRate0 = SoundRead(ff)
            # resampling 
            if (SampleRate0 != SampleRateRef):
                Samples0 = signal.resample(Samples0, float(len(Samples0))/SampleRate0*SampleRateRef)
            
            # make single-channel sound multi-dimensional array
            if(Samples0.ndim == 1):
                #Samples0 = np.array([Samples0]).T
                Samples0 = Samples0[:,None]
            
            for cc in range(Samples0.shape[1]):            
            #for cc in range(2, 3): # channel number
                ##print "Channel "+str(cc)
                
                ss_last = int(np.floor(Samples0.shape[0]*1.0/SampleRateRef/SegLen))
                #for ss in range(ss_last+1): # fixed the problem on SampleRate. Use SampleRate0 mistakenly at first.
                for ss in range(ss_last): 
                #for ss in range(2):
                    #if ss == ss_last-1:
                    #    Samples = Samples0[ss*SegLen*SampleRateRef:,cc]
                    #else:    
                    #    Samples = Samples0[ss*SegLen*SampleRateRef:(ss+1)*SegLen*SampleRateRef,cc]
                    Samples = Samples0[ss*SegLen*SampleRateRef:(ss+1)*SegLen*SampleRateRef,cc]
                    Samples = Samples - Samples.mean()
                    
                    # STFT
                    SftfMat = np.abs(librosa.stft(Samples, n_fft=FFTSize, hop_length=HopSize))
                    PowerLawStft = PowerLawMatCal(SftfMat, Nu1, Nu2, Gamma) # over time, freq
                    ##TDetecFunc = PowerLawStft.sum(axis=0) # summation over all frequency!?
                    TDetecFunc = PowerLawStft[IndFreqLow:IndFreqHigh,:].sum(axis=0) # summation over all frequency!?
                    TDetecRange = (TDetecFunc > TDetecThre).astype(int)
                    
                    TDetecRangeDiff = np.diff(TDetecRange)
                    IndDiff, ValDiff = FindIndexNonZero(TDetecRangeDiff)
                    
                    EventList = []
                    if ValDiff: # test if there's any detection result
                        if(ValDiff[0] == -1):
                            ValDiff = [1] + ValDiff
                            IndDiff = [0] + IndDiff
                        if(ValDiff[-1] == 1):
                            ValDiff.append(-1)
                            IndDiff.append(len(TDetecRangeDiff))
                        # Feature extraction based on detection results
                        for tt in range(0, len(ValDiff), 2):
                            TimeE1 = IndDiff[tt]
                            TimeE2 = (IndDiff[tt+1]+1)
                            TimeE_Cen = int(np.floor((IndDiff[tt]+(IndDiff[tt+1]+1))/2.))                            
                            
                            #if (TDetecThreLen <= (TimeE2-TimeE1)*EventTimeReso) and (TimeE_Cen-TimeE_DurHalf >= 0) and (TimeE_Cen+TimeE_DurHalf<TDetecRangeDiff.shape[0]):
                            if (TDetecThreLen <= (TimeE2-TimeE1)*EventTimeReso) and (TimeE_Cen-TimeE_DurHalf >= 0) and (TimeE_Cen+TimeE_DurHalf<TDetecRangeDiff.shape[0]):
                                #EventTVal = np.max(TDetecFunc[IndDiff[tt]:(IndDiff[tt+1]+1)]) # This is the score! It is the max value in the detection function.
                                EventTVal = np.percentile(TDetecFunc[IndDiff[tt]:(IndDiff[tt+1]+1)], 75) # This is the score! It is the max value in the detection function.
                                ImageVecCurr = PowerLawStft[IndFreqLow:IndFreqLow+200,TimeE_Cen-TimeE_DurHalf:TimeE_Cen+TimeE_DurHalf]
                                EventList.append([TimeE1,TimeE2,EventTVal, ImageVecCurr.flatten()])
                        # write to selection table
                        for ee in range(len(EventList)):
                            # write to selection table
                            EventId += 1
                            Time1 = TimeCurr.hour*3600 + TimeCurr.minute*60.0 + TimeCurr.second + ss*SegLen*1.0 + EventList[ee][0]*EventTimeReso
                            Time2 = TimeCurr.hour*3600 + TimeCurr.minute*60.0 + TimeCurr.second + ss*SegLen*1.0 + EventList[ee][1]*EventTimeReso
                            TimeOffset = ss*SegLen*1.0 + EventList[ee][0]*EventTimeReso
                            f.write(str(EventId)+'\t'+'Spectrogram'+'\t'+str(cc+1)+'\t'+str.format("{0:=.4f}",Time1)+'\t'+str.format("{0:<.4f}",Time2)+'\t'+str.format("{0:=.1f}",50.0)+'\t'+str.format("{0:=.1f}",800.0)+'\t'+str.format("{0:<.5f}",EventList[ee][2])+'\t'+str.format("{0:<.5f}",Time2-Time1)+'\t'+ff+'\t'+str.format("{0:=.4f}",TimeOffset) )
                            #f.write('Selection\tView\tChannel\tBegin Time (s)\tEnd Time (s)\tLow Freq (Hz)\tHigh Freq (Hz)\tScore\tBegin Path\tFile Offset (s)')
                            f.write('\n')
                            ## collect image vectors
                            ImageVecList.append(EventList[ee][3])
            #ffCount += 1
        f.close()
        ## write to numpy array files / .npy
        np.save(os.path.join(SelTabPath+'/',os.path.basename(DaySound)+'.npy'), np.array(ImageVecList))
        del ImageVecList

        return os.path.basename(DaySound)+' was processed.'
    else:
        return os.path.basename(DaySound)+' already exists.'

def TriWinSum(x, N) :
    # N needs to be of an odd number
    M = (N+1)/2
    TriWin = np.hstack((np.cumsum(np.ones(M))/M, (M-np.cumsum(np.ones(M)))/M))
    TriWin = TriWin[:-1]
    return np.convolve(x, TriWin, mode='same')    

def MeanSpecFea(CqtCurr, OctLow, OctHigh, DeciFac):
    MeanSpec0 = ((CqtCurr.mean(axis=1))[OctLow:OctHigh]+1)**(.5)-1 # 3 octaves;
    MeanSpecCurr = TriWinSum(MeanSpec0, 5).T
    return MeanSpecCurr[::DeciFac] #signal.decimate(FeaCurr, DeciFac)

def EntropyMovWin(CqtGrayMap, MovWin, MagLen):
    CqtGrayMapCurr = np.zeros((CqtGrayMap.shape[0]-MovWin+1, CqtGrayMap.shape[1]-MovWin+1))
    for ii in range(CqtGrayMapCurr.shape[0]):
        for jj in range(CqtGrayMapCurr.shape[1]):
            CqtGrayMapCurr[ii, jj] = CqtGrayMap[ii:ii+MovWin, jj:jj+MovWin].astype(float).mean()
    CqtGrayCount = np.histogram(CqtGrayMapCurr.flatten(), bins = np.array(range(MagLen+1))-0.5 )[0]
    EntroMag1 = entropy(CqtGrayCount, base=2)
    return CqtGrayMapCurr, EntroMag1

def Cqt2TDetec(PLCqt, TDetecSmooth, Winn):
    TDetecFuncX = np.mean(PLCqt, axis=0)
    TDetecFuncX = signal.medfilt(TDetecFuncX, TDetecSmooth)
    TDetecFuncX = np.convolve(TDetecFuncX, Winn, mode='same')
    return TDetecFuncX

if __name__ == "__main__":
    
    PROJ = 1 # CHAOZ-X
    #TEST_SNGLE_FILE = True
    TEST_SNGLE_FILE = False
    SingleDep = True # True: single deployment; False: multiple deployment
    
    #SoundDir = r'/media/sf_2013_WHOI_Chukchi_71664/71664_WHOI01_20130820/71664_WHOI01_AIFF_PU197_2XB'
    #71664_WHOI01_PU197_2XB_20130912
    
    if SingleDep: # Single Deployment
        if PROJ == 1: # CHAOZ-X
            #WorkPath = r'/media/sf_2013_WHOI_Chukchi_71664/71664_WHOI01_20130820/71664_WHOI01_AIFF_PU197_2XB'
            #SelTabPath = r'/media/sf___ASA_Boston/__CHAOZX/2013'
            WorkPath = r'N:\projects\2013_WHOI_Chukchi_71664\71664_WHOI01_20130820\71664_WHOI01_AIFF_PU197_2XB'
            #SelTabPath = r'/media/sf___ASA_Boston/__CHAOZX/2013'
            SelTabPath = r'P:\users\yu_shiu_ys587\__Soundscape\__ASA_Boston\__CHAOZX\2013'
            
        elif PROJ == 2: # New Zealand
            WorkPath = r'/media/sf_S1068_NZ01_201612/S1068_NZ01_Swift_FLAC'
            SelTabPath = r'/media/sf___ASA_Boston/__NewZealand'
        elif PROJ == 3: # SSW
            WorkPath = r'/media/sf_2016_BRP_SSW_S1067/S1067_SSW04_201703/S1067_SSW04_201703_FLAC'
            SelTabPath = r'/media/sf___ASA_Boston/__SSW'
            
        WorkPath = WorkPath.replace('\\','/')
        DayList = sorted(glob.glob(os.path.join(WorkPath+'/','*')))        
        # Test selected days
        #DayList = DayList[20:210:30]
        SelTabPathList = [SelTabPath] * len(DayList) # make it a list for imap_unordered use            
    else: # multiple Deployment
        WorkPath = [r'/media/sf_2013_WHOI_Chukchi_71664/71664_WHOI01_20130820/71664_WHOI01_AIFF_PU197_2XB',
                    r'/media/sf_2013_WHOI_Chukchi_71664/71664_WHOI01_20130820/71664_WHOI01_AIFF_PU197_2XB']

        SelTabPath = [r'/media/sf___ASA_Boston/__CHAOZX/2013',
                      r'/media/sf___ASA_Boston/__CHAOZX/2014']
        
        DayList = []
        SelTabPathList = []
        for ww in range(len(WorkPath)):
            WorkPathCurr = WorkPath[ww]
            WorkPathCurr = WorkPathCurr.replace('\\','/')
            DayListCurr = sorted(glob.glob(os.path.join(WorkPathCurr+'/','*')))
            DayList = DayList + DayListCurr
            SelTabPathList = SelTabPathList + [SelTabPath[ww]] * len(DayListCurr)

    if TEST_SNGLE_FILE == True:
        #TheDay = 11
        TheDay = 23
        print DayList[TheDay]
        ReturnedStr = DetectViaPowerLaw(DayList[TheDay], SelTabPathList[TheDay])
        #for ii in range(7):
        #   print DayList[ii]
        #    DetectViaPowerLaw(DayList[ii], SelTabPathList[ii])
    else:
        MyPool = Pool(7) 
        try:
            for ReturnedStr in MyPool.imap_unordered(universal_worker, pool_args(DetectViaPowerLaw, DayList, SelTabPathList)):
            #for ReturnedStr in MyPool.imap_unordered(universal_worker, pool_args(DetectViaPowerLaw, DayList2, SelTabPathList)):
                #print ReturnedStr
                logging.info(ReturnedStr)
            #print "Waiting 10 seconds"
            time.sleep(10)
        except KeyboardInterrupt:
            print "Caught KeyboardInterrupt, terminating workers"
            MyPool.terminate()
            MyPool.join()
        else:
            print "Work is finished. Quitting gracefully"
            MyPool.close()
            MyPool.join()
            