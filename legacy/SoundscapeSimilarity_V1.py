# -*- coding: utf-8 -*-
"""
Soundscape similarity
For all days

Assuming clock is right

Created on Wed Apr 19 15:16:42 2017

Quantify the spectrogram/soundscape
Measure the distance
Draw t-SNE figure

@author: ys587
"""
import numpy as np
import os
import matplotlib.pyplot as plt
from sklearn.manifold import TSNE
from sklearn.decomposition import PCA
from scipy.cluster.hierarchy import dendrogram, linkage

if __name__ == "__main__":
    SpectroInPath = r'P:\users\yu_shiu_ys587\__SoundScape\__NewZealand\__SpecData' # freq resolution 10Hz
    #FigOutPath = r'C:\ASE_Data\Box Sync\__Project\__NewZealand\__DetectClustering'
    #FigOutPath = r'C:\ASE_Data\Box Sync\__Project\__NewZealand\__DetectClustering\__DawnChorusSpectroSimilarity'
    FigOutPath = r'C:\ASE_Data\Box Sync\__Project\__NewZealand\__DetectClustering\__DawnChorusSpectroSimilarity_24hours'
    
    DeployName = 'S1068NZ01'
    # Site Format SXX. E.g. S05, S10
    DayList = ['20161210', '20161211', '20161212', '20161213', '20161214', '20161215', '20161216', '20161217', '20161218']
    NumSite = 10

    # Given target day, read spectrogram from all sites    
    print "Read spectrogram of the target day from all sites"
    
    #dd = 1 # 20161211 ### <<<<<<< ===========
    #DayTarget = 2 # 20161212
    #DayTarget = 6
    for dd in range(len(DayList)):
        print "Target day: "+str(DayList[dd])
        SiteDaySpectro = []
        for ss in range(NumSite):
            #print ss
            SiteDaySpectro.append(np.load(os.path.join(SpectroInPath, DeployName+'_'+str(ss+1)+'_'+DayList[dd]+'.npy' )))
        
        # Feature / fingerprint
        FreqReso = 10. # data has 10-Hz resolution
        FreqStart = 1000. # Hz
        FreqStop = 10000. # Hz
        FreqStartInd = int(np.floor(FreqStart/FreqReso))
        FreqStopInd = int(np.floor(FreqStop/FreqReso)) - 1
        FreqResoScalar = 10 # convert frequency resolution from 10 Hz to 100 Hz. Thus, 10-fold,
        FreqIndList = range(FreqStartInd, FreqStopInd, FreqResoScalar)
        
        print "Calculating fingerprinting..."
        NumFreqBand = len(FreqIndList)
        
        # data have 30-sec resolution
        #StartTime = 15*60.*2. # default 0
        #EndTime = 17.5*60.*2. # default 2880 (=1440min*60sec/30sec resolution)
        StartTime = 0*60.*2. # default 0
        EndTime = 23.5*60.*2. # default 2880 (=1440min*60sec/30sec resolution)
        #NumTime = 2880
        NumTime = int(EndTime - StartTime)
        
        #NumTime = 1000
        FeaSoundScapeSite = []
        #FeaLoudness = np.zeros(NumSite)
        for ss in range(NumSite):
        #for ss in range(2):
            print "Site: %d" % ss
            #FeaSoundScape = np.zeros([(NumFreqBand-1)*NumTime])
            FeaSoundScape = np.zeros([(NumFreqBand-1)])
            
            # draw SiteDaySpectro[ss][FreqIndList[0]:FreqIndList[-1],StartTime:EndTime] for the time range
            
            if False:
                # 2 mistakes
                # 1. int(tt-StartTime) should be int(tt+StartTime)
                # 2. the bird call changes very fast. 30-sec resolution cannot catch its time-varying characteristics.
                for ff in range(NumFreqBand-1):
                    #print "Band: %d" % ff
                    for tt in range(NumTime):
                        FeaSoundScape[ff*NumTime+tt] = (SiteDaySpectro[ss])[FreqIndList[ff]:FreqIndList[ff+1], int(tt-StartTime)].mean()
            
            for ff in range(NumFreqBand-1):
                FeaSoundScape[ff] = (SiteDaySpectro[ss])[FreqIndList[ff]:FreqIndList[ff+1], int(StartTime):int(StartTime+NumTime)].mean()
                
                    
            #FeaLoudness[ss] = FeaSoundScape.sum()
            FeaSoundScape = ((1.0 / (1. + np.exp(-1*FeaSoundScape))) - 0.5)*2.0
            #FeaSoundScape = FeaSoundScape - FeaSoundScape.mean()
            FeaSoundScape /= ((FeaSoundScape**2.).sum())**.5
            FeaSoundScapeSite.append(FeaSoundScape)
        FeaSoundScapeSiteArr = np.vstack(FeaSoundScapeSite)
            
        # Similarity: time resolution 2 hours
        SimiSite = np.zeros([NumSite, NumSite])
        for ss1 in range(NumSite):
            for ss2 in range(NumSite):
                SimiSite[ss1, ss2] = (FeaSoundScapeSite[ss1]*FeaSoundScapeSite[ss2]).sum()
        
    
        #################################################################################################        
        # Draw clustering tree
        labelList = ['S1', 'S2', 'S3', 'S4', 'S5', 'S6', 'S7', 'S8', 'S9', 'S10']
        Z = linkage(FeaSoundScapeSiteArr, method='average',metric='cosine')
        plt.figure(figsize=(15, 8))
        plt.title(DayList[dd]+': Hierarchical Clustering Dendrogram')
        plt.xlabel('sample index')
        plt.ylabel('distance')
        dendrogram(Z, 
        leaf_rotation=0.,  # rotates the x axis labels
        leaf_font_size=14.,  # font size for the x axis labels
        labels=labelList,
        )
        #plt.show()
        plt.savefig(os.path.join(FigOutPath, DayList[dd]+'_Dendrogram.png'))
        #################################################################################################        
        plt.figure(figsize=(15, 8))
        plt.subplot(211)
        
        for ii in range(5):
            plt.plot(np.arange(float(NumFreqBand-1))*FreqResoScalar*FreqReso+FreqStart, FeaSoundScapeSite[ii], label='S'+str(ii+1))
        plt.legend()
        plt.title(DayList[dd]+': Island')
        plt.subplot(212)
        for ii in range(5):
            plt.plot(np.arange(float(NumFreqBand-1))*FreqResoScalar*FreqReso+FreqStart, FeaSoundScapeSite[ii+5],label='S'+str(ii+5+1))
        plt.legend()
        plt.title(DayList[dd]+': Peninsula')
        #plt.show()
        plt.savefig(os.path.join(FigOutPath, DayList[dd]+'_Spectrum.png'))
    

    

    
# Notes from Underground
# np.save(os.path.join(VisOutputPath, DeployName+'_'+str(ss+1)+'_'+DayList[dd]), Day_Spectro)