# -*- coding: utf-8 -*-
"""
Created on Thu Aug 24 10:04:51 2017

*Following V1_2 to calculate the long-term spectrogram
*Adding the time bins to align the energy data

New Zealand Long-Term Spectrogram (CQT) for every site and day
# V1_2: calculate the CQT and save them; drawing will be handled by V1_3
# V1_3: draw figures for each day of multi-sites instead of each site of multi-days
# Assumption: 
(1) all sites have identical range of days; 
(2) the list of days will come from site 1; 
(3) we'll deal with non-identical range of days later but sorting the filenames

# V1_1: move most works into a function, in order to release memory
# Memory issue: out of memory after 2 site figures V1

To-do-jobs
1. long-term spectrogram or cqt-based spectrogram (freq vs day)
2. power-law-based onset events (hourly count vs day)
3. freq-band events (frer-band count vs time)

Steps:
1. Check the time from the filenames for all the units.
2. Find the common/ overlapped duration
3. draw individual STFT <<== start with this

Time: 
1. focus the dates between 20160210 - 20160219
2. 10 days!
3. UTC time: New Zealand time is UTC/GMT +12 hours for stanard time and +13 for daylight saving time
4. Time is not precise. Neet to find a way to store the FFT results

Challenges:
1. Need to find a way of selecting the expected days and time for the analysis and ignoring others

    
@author: ys587
"""
import os, glob
import time
import sys
import soundfile as sfile
import librosa as rosa
import matplotlib.pyplot as plt
import numpy as np
import re
import datetime

FLAG_FIG = False
#FLAG_DEBUG = True
#TEST_SNGLE_FILE = False
SPEC_METHOD = 'stft'  # ['cqt', 'stft']

# For STFT
FreqReso = 10 # 10 Hz per point
# Hop length affects the time resolution
FsRef  = 32000
NumFFT = FsRef/FreqReso
NumFFTHalf = NumFFT/2 + 1

#TimeScaleSec = 120. # 2 min
TimeScaleSec = 60. # 1 min

regex = re.compile("_(\d{8})$") # YYYYMMDD

TIME_FORMAT = "%Y%m%d_%H%M%S" # this is how your timestamp looks like
regex = re.compile("_(\d{8}_\d{6})") # YYYYMMDD_HHMMSS
#regex1 = re.compile("_(\d{2})(\d{2})(\d{2})$") # YYYYMMDD_HHMMSS
#regex1 = re.compile("_(\d{2})(\d{2})(\d{2})_") # YYYYMMDD_HHMMSS                 

def GetTimeStamp(TheString):
    m = regex.search(TheString)
    return datetime.datetime.strptime(m.groups()[0], TIME_FORMAT)

def DaySpectroCalcSTFTSimple(DayName, TimeScaleSec):
    Samples, Fs = sfile.read(DayName)
    Day_Spectrogram0 = abs(rosa.stft(Samples, n_fft=NumFFT, hop_length=Fs))

    Day_Spectrogram_DimT = int(np.floor(Day_Spectrogram0.shape[1] / TimeScaleSec))
    Day_Spectrogram = np.zeros([Day_Spectrogram0.shape[0], Day_Spectrogram_DimT])
    for ii in range(Day_Spectrogram_DimT):
        Day_Spectrogram[:, ii] = Day_Spectrogram0[:, ii * TimeScaleSec:(ii + 1) * TimeScaleSec].mean(axis=1)
    return Day_Spectrogram


def OneSiteDaySpec(TargetDay, TimeScaleSec, SPEC_FLAG):
    FileList = sorted(glob.glob(os.path.join(TargetDay+'/','*')))
    
    print "Calculating spectrogram..."
    DaySpectrogramAbs = []
    for ii in range(len(FileList)): # sound files in a single day
    #for ii in range(2): #############################<<<<<<<<<<<<<<<<<<<<<<<<<<===================================
        if ii % 1 == 0:                    
            print "Sound file from " + str(ii) + "..."
        DaySpectrogramAbs.append(DaySpectroCalcSTFTSimple(FileList[ii], TimeScaleSec))

    print "Merging..."
    DaySpectrogramAbsTot = np.hstack(DaySpectrogramAbs)
    del DaySpectrogramAbs
    print "DaySpectrogramAbsTot shape: " + str(DaySpectrogramAbsTot.shape)
    
    return DaySpectrogramAbsTot
                
if __name__ == "__main__":
    SitePathBase = r'P:\users\yu_shiu_ys587\__SoundScape\__NewZealandData\S1068_NZ01_201612_UniformDays'
    VisOutputPath = r'P:\users\yu_shiu_ys587\__SoundScape\__NewZealand\__SpecData3'
    
    DeployName = 'S1068NZ01'
    # Site Format SXX. E.g. S05, S10
    DayList = ['20161210', '20161211', '20161212', '20161213', '20161214', '20161215', '20161216', '20161217', '20161218']
    NumDay = len(DayList)
    NumOfSite = 10
    DayFirst = datetime.datetime(2016,12,10,0,0,0)

    if SPEC_METHOD == 'stft':
        SPEC_FLAG = 0
    elif SPEC_METHOD == 'cqt':
        SPEC_FLAG = 1
    else:
        print 'Spectrogram method is not supported'
        sys.exit(0)

    # Basename: SiteNumber + Date
    # e.g. S02_20161213 or S10_20161216

    # matrix holding all the day-site folder names
    # 2D list: NumOfSite xNumOfDay
    SiteDayList = [[None for x in range(NumDay)] for y in range(NumOfSite)]
    for ss in range(NumOfSite):
        if ss+1 != NumOfSite:
            SiteDayList[ss] = sorted(glob.glob(os.path.join(SitePathBase, DeployName+'_S0'+str(ss+1), '*')))
        else: #SiteNum==10
            SiteDayList[ss] = sorted(glob.glob(os.path.join(SitePathBase, DeployName+'_S'+str(ss+1), '*')))
    
    #TimeReso = 100 # 10 sec
    #Spectro = np.zeros([NumFFTHalf, int(86400.*NumDay/TimeReso), NumOfSite-1])
    #SpectroSiteAll = []
    for ss in range(NumOfSite-1):
        SpectroSite = np.zeros([NumFFTHalf, int(86400.*NumDay/TimeScaleSec)])
        #SpectroSite = np.zeros([NumFFTHalf, int(86400.*NumDay)])
        for dd in range(NumDay):
            print 'dd: ' + str(DayList[dd]),
            print 'ss: '+str(ss)
            FileList = sorted(glob.glob(os.path.join(SiteDayList[ss][dd]+'/','*')))
            #for ff in range(len(FileList)):
            for ff in range(3):
                print 'ff: '+str(ff),
                ff2 = os.path.splitext(os.path.basename(FileList[ff]))[0]
                TimeCurr = GetTimeStamp(ff2)
                
                Samples, Fs = sfile.read(FileList[ff])
                Samples = Samples - Samples.mean()
                Day_Spectrogram0 = abs(rosa.stft(Samples, n_fft=NumFFT, hop_length=Fs))
                
                #jj = max(CurrTime.second, )
                #while():
                #Day_Spectrogram0[:,TimeScaleSec-]
                StartInd = int(np.floor(((TimeCurr.day-DayFirst.day)*86400.+TimeCurr.hour*3600.+TimeCurr.minute*60.+TimeCurr.second)/TimeScaleSec))
                jj = 0
                #while(jj*TimeScaleSec <=Samples.shape[0]/Fs):
                while(jj <= 3):
                    #print 'jj: '+str(jj),
                    SpectroSite[:,StartInd+jj] = np.average(Day_Spectrogram0[:,jj*TimeScaleSec:(jj+1)*TimeScaleSec], axis=1)
                    jj += 1
                #SpectroSite[:,StartInd:StartInd+Samples.shape[0]/Fs] = Day_Spectrogram0[:, 0:Samples.shape[0]/Fs]
                #t1 = DaySpectroCalcSTFTSimple(FileList[ff], TimeScaleSec)
                #SpectroSite[:, ] = DaySpectroCalcSTFTSimple(FileList[ff], TimeScaleSec)
            print ""
        np.save(os.path.join(VisOutputPath, DeployName+'_'+str(ss+1)), SpectroSite)
        #DaySpectrogramAbs.append()
        #SpectroSiteAll.append(SpectroSite)



    if False:
        plt.plot(SpectroSite[:,96*15+1]); plt.show()



    
    
    if False:
        # delay stat
        Delay = []
        #print 'dd: ' + str(dd)
        #print 'dd: ' + str(DayList[dd])
        for ss in range(NumOfSite-1): # exclude the 10th recorder
        #for ss in range(NumOfSite-1, NumOfSite): # only the 10th recorder
        #for ss in range(4):
            for dd in range(NumDay):
            #for dd in range(3):
                print 'dd: ' + str(DayList[dd]),
                print 'ss: '+str(ss)
                FileList = sorted(glob.glob(os.path.join(SiteDayList[ss][dd]+'/','*')))
                #print len(FileList)
                #for ff in FileList:
                for ff in range(len(FileList)):
                    ff2 = os.path.splitext(os.path.basename(FileList[ff]))[0]
                    #print ff2
                    TimeCurr = GetTimeStamp(ff2)
                    #print 'Time Curr: '
                    #print TimeCurr, 
                    if(ff == 0):
                        TimePrev = GetTimeStamp(ff2)
                    else:
                        TimeDiff = TimeCurr - TimePrev
                        TimePrev = TimeCurr
                        if np.abs(TimeDiff.seconds-900)>=1.0:
                            print ff2,
                            print TimeCurr, 
                            print 'Time Diff: ',
                            print TimeDiff.seconds - 900.,
                            Delay.append(TimeDiff.seconds - 900.)
                            print ''
        print ''
            
