# -*- coding: utf-8 -*-
"""
Soundscape similarity
For a single day

Assuming clock is right

Created on Wed Apr 19 15:16:42 2017

Quantify the spectrogram/soundscape
Measure the distance
Draw t-SNE figure

@author: ys587
"""
import numpy as np
import os
import matplotlib.pyplot as plt
from sklearn.manifold import TSNE
from sklearn.decomposition import PCA
from scipy.cluster.hierarchy import dendrogram, linkage

if __name__ == "__main__":
    SpectroInPath = r'P:\users\yu_shiu_ys587\__SoundScape\__NewZealand\__SpecData' # freq resolution 10Hz
    #FigOutPath = r'C:\ASE_Data\Box Sync\__Project\__NewZealand\__DetectClustering'
    FigOutPath = r'C:\ASE_Data\Box Sync\__Project\__NewZealand\__DetectClustering\__DawnChorusSpectroSimilarity'
    
    DeployName = 'S1068NZ01'
    # Site Format SXX. E.g. S05, S10
    DayList = ['20161210', '20161211', '20161212', '20161213', '20161214', '20161215', '20161216', '20161217', '20161218']
    NumSite = 10

    # Given target day, read spectrogram from all sites    
    print "Read spectrogram of the target day from all sites"
    
    #DayTarget = 1 # 20161211 ### <<<<<<< ===========
    #DayTarget = 2 # 20161212
    DayTarget = 6
    
    print "Target day: "+str(DayList[DayTarget])
    
    SiteDaySpectro = []
    for ss in range(NumSite):
        #print ss
        SiteDaySpectro.append(np.load(os.path.join(SpectroInPath, DeployName+'_'+str(ss+1)+'_'+DayList[DayTarget]+'.npy' )))
    
    # Feature / fingerprint
    FreqReso = 10. # data has 10-Hz resolution
    FreqStart = 1000. # Hz
    FreqStop = 10000. # Hz
    FreqStartInd = int(np.floor(FreqStart/FreqReso))
    FreqStopInd = int(np.floor(FreqStop/FreqReso)) - 1
    FreqResoScalar = 10 # convert frequency resolution from 10 Hz to 100 Hz. Thus, 10-fold,
    FreqIndList = range(FreqStartInd, FreqStopInd, FreqResoScalar)
    
    print "Calculating fingerprinting..."
    #NumFreqBand = int(np.floor(SiteDaySpectro[0].shape[0]/float(FreqResoScalar)))
    #NumFreqBand = FreqStopInd - FreqStartInd + 1
    NumFreqBand = len(FreqIndList)
    
    # data have 30-sec resolution
    ## All day
    #StartTime = 0*60.*2. # default 0
    #EndTime = 24.0*60.*2. # default 2880 (=1440min*60sec/30sec resolution)
    
    ## Dawn chorus
    StartTime = 15*60.*2. # default 0
    EndTime = 17.5*60.*2. # default 2880 (=1440min*60sec/30sec resolution)
    
    #NumTime = 2880
    NumTime = int(EndTime - StartTime)
    
    #NumTime = 1000
    FeaSoundScapeSite = []
    #FeaLoudness = np.zeros(NumSite)
    for ss in range(NumSite):
    #for ss in range(2):
        print "Site: %d" % ss
        #FeaSoundScape = np.zeros([(NumFreqBand-1)*NumTime])
        FeaSoundScape = np.zeros([(NumFreqBand-1)])
        
        # draw SiteDaySpectro[ss][FreqIndList[0]:FreqIndList[-1],StartTime:EndTime] for the time range
        
        if False:
            # 2 mistakes
            # 1. int(tt-StartTime) should be int(tt+StartTime)
            # 2. the bird call changes very fast. 30-sec resolution cannot catch its time-varying characteristics.
            for ff in range(NumFreqBand-1):
                #print "Band: %d" % ff
                for tt in range(NumTime):
                    #FeaSoundScape[ff*NumTime+tt] = (SiteDaySpectro[ss])[ff*FreqResoScalar:(ff+1)*FreqResoScalar, tt].mean() # from zero time to NumTime
                    #FeaSoundScape[ff*NumTime+tt] = (SiteDaySpectro[ss])[ff*FreqResoScalar:(ff+1)*FreqResoScalar, int(tt-StartTime)].mean()
                    FeaSoundScape[ff*NumTime+tt] = (SiteDaySpectro[ss])[FreqIndList[ff]:FreqIndList[ff+1], int(tt-StartTime)].mean()
        
        for ff in range(NumFreqBand-1):
            FeaSoundScape[ff] = (SiteDaySpectro[ss])[FreqIndList[ff]:FreqIndList[ff+1], int(StartTime):int(StartTime+NumTime)].mean()
            
                
        #FeaLoudness[ss] = FeaSoundScape.sum()
        FeaSoundScape = ((1.0 / (1. + np.exp(-1*FeaSoundScape))) - 0.5)*2.0
        #FeaSoundScape = FeaSoundScape - FeaSoundScape.mean()
        FeaSoundScape /= ((FeaSoundScape**2.).sum())**.5
        FeaSoundScapeSite.append(FeaSoundScape)
    FeaSoundScapeSiteArr = np.vstack(FeaSoundScapeSite)

    
#    NumTime = 2880
#    for ss in range(len(SiteDaySpectro)):
#        NumTime = np.min([NumTime, SiteDaySpectro[ss].shape[1]])
#    
#    #NumTime = 1000
#    FeaSoundScapeSite = []
#    FeaLoudness = np.zeros(NumSite)
#    for ss in range(NumSite):
#    #for ss in range(2):
#        print "Site: %d" % ss
#        
#        FeaSoundScape = np.zeros([NumFreqBand*NumTime])
#        for ff in range(NumFreqBand):
#            #print "Band: %d" % ff
#            for tt in range(NumTime):
#                FeaSoundScape[ff*NumTime+tt] = (SiteDaySpectro[ss])[ff*FreqResoScalar:(ff+1)*FreqResoScalar, tt].mean()
#        
#        FeaLoudness[ss] = FeaSoundScape.sum()
#        FeaSoundScape = ((1.0 / (1. + np.exp(-1*FeaSoundScape))) - 0.5)*2.0
#        #FeaSoundScape = FeaSoundScape - FeaSoundScape.mean()
#        FeaSoundScape /= ((FeaSoundScape**2.).sum())**.5
#        FeaSoundScapeSite.append(FeaSoundScape)
#    FeaSoundScapeSiteArr = np.vstack(FeaSoundScapeSite)
        
    # Similarity: time resolution 2 hours
    SimiSite = np.zeros([NumSite, NumSite])
    for ss1 in range(NumSite):
        for ss2 in range(NumSite):
            SimiSite[ss1, ss2] = (FeaSoundScapeSite[ss1]*FeaSoundScapeSite[ss2]).sum()
    

    #################################################################################################        
    # Draw clustering tree
    labelList = ['S1', 'S2', 'S3', 'S4', 'S5', 'S6', 'S7', 'S8', 'S9', 'S10']
    Z = linkage(FeaSoundScapeSiteArr, method='average',metric='cosine')
    plt.figure(figsize=(15, 8))
    plt.title('Hierarchical Clustering Dendrogram')
    plt.xlabel('sample index')
    plt.ylabel('distance')
    dendrogram(Z, 
    leaf_rotation=0.,  # rotates the x axis labels
    leaf_font_size=14.,  # font size for the x axis labels
    labels=labelList,
    )
    #plt.show()
    plt.savefig(os.path.join(FigOutPath, DayList[DayTarget]+'_Dendrogram.png'))
    #################################################################################################        
    plt.figure(figsize=(15, 8))
    plt.subplot(211)
    for ii in range(5):
        plt.plot(FeaSoundScapeSite[ii],label='S'+str(ii+1))
    plt.legend()
    plt.title(DayList[DayTarget]+': Island')
    plt.subplot(212)
    for ii in range(5):
        plt.plot(FeaSoundScapeSite[ii+5],label='S'+str(ii+5+1))
    plt.legend()
    plt.title(DayList[DayTarget]+': Peninsula')
    #plt.show()
    plt.savefig(os.path.join(FigOutPath, DayList[DayTarget]+'_Spectrum.png'))

    #################################################################################################            
    # TSNE failure! Figure of every day looks exactly identical. It's seriously effected by random state and seems have nothing to do with distance matrix
    if False:
        # TSNE
        model = TSNE(n_components = 2, random_state = 0,perplexity=30.0)
        np.set_printoptions(suppress=True)
        Y = model.fit_transform(FeaSoundScapeSiteArr)
        #fig, ax = plt.subplots(1, 1)
    
        # Draw
        CircleArea = np.log(FeaLoudness)
        CircleArea = CircleArea - CircleArea.min() + 1.0
        
        data = np.random.random((NumSite, 2))
        #labels = ['point{0}'.format(i) for i in range(NumSite)]
        Labels = ['S'+str(ss+1) for ss in range(NumSite)]
        plt.subplots_adjust(bottom = 0.1)
        plt.scatter(
            Y[:, 0], Y[:, 1], marker='o', c=data[:, 0], s=data[:, 1]*1000,
            cmap=plt.get_cmap('Spectral'))
        # s=FeaLoudness*10000
        Y0Dist = Y[:,0].max() - Y[:,0].min()
        Y1Dist = Y[:,1].max() - Y[:,1].min()
        plt.xlim([Y[:,0].min()-0.25*Y0Dist, Y[:,0].max()+0.25*Y0Dist])
        plt.ylim([Y[:,1].min()-0.25*Y1Dist, Y[:,1].max()+0.25*Y1Dist])
        
        for label, x, y in zip(Labels, Y[:, 0], Y[:, 1]):
            plt.annotate(
                label,
                xy=(x, y), xytext=(-20, 20),
                textcoords='offset points', ha='right', va='bottom',
                bbox=dict(boxstyle='round,pad=0.5', fc='yellow', alpha=0.5),
                arrowprops=dict(arrowstyle = '->', connectionstyle='arc3,rad=0'))
        plt.show()
    

    
# Notes from Underground
# np.save(os.path.join(VisOutputPath, DeployName+'_'+str(ss+1)+'_'+DayList[dd]), Day_Spectro)