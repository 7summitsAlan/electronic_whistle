# -*- coding: utf-8 -*-
"""
Created on Sat Jun 17 21:40:05 2017

@author: ys587
"""
#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""

Extract images for learning representation in tensorflow
3 datasets: (i) __CHAOZX (ii) __NewZealand (iii) __SSW

cropped images / data augmentation / scale?

output: (i) selection table; (ii) images or waveform?

Goals: 
1. display long-term dataset
2. display short-term acoustic activity such as seiemic airgun on 20130902
periods of airgun pulses: 4 sec, 1 min. 
period of "gunshot" pulses: 3 min

Created on Mon Jun 12 11:38:56 2017

@author: atoultaro
"""
#from CHAOZ_ExtractImage_V0 import *
from CHAOZ_ExtractImage_V0 import GetTimeStamp, SoundRead, PowerLawMatCal, PoweLawFindMu, FindIndexNonZero, universal_worker, pool_args
import numpy as np
import glob, os
import logging
import scipy.signal as signal
import librosa
from multiprocessing import Pool #Process, Queue,
#import itertools
import time
#import matplotlib.pyplot as plt

logging.basicConfig(filename=r'P:\users\yu_shiu_ys587\__Soundscape\__ASA_Boston\__SSW\S1067_SSW04_201703\running2017.log', level=logging.INFO)

FFTSize = 2048
SampleRateRef = 48000
TimeE_Dur = 1.0 # sec

Nu1 = 1.0
Nu2 = 2.0
Gamma = 1.0
SegLen = 300 # 300sec/5 min
# HopLength = 2**6 # step size
TDetecThre = 1.5e-4

TDetecSmooth = 9 # median filter window for smoothing TDetec function; window length = 9*.032=
TDetecThreLen = 0.15

# FFT
#FFTSize = 512
#FFTSize = 256
WinSize = FFTSize
HopSize = 1920 # 40 msec
#HopSize = 960 # 20 msec
EventTimeReso = HopSize/float(SampleRateRef) # step size in sec
TimeE_DurHalf = int(np.floor(TimeE_Dur/2/EventTimeReso)) # int

IndFreqLow = int(np.floor(500./SampleRateRef*FFTSize)) # used for both GPL power as well as the spectrogram image starting point
IndFreqHigh = int(np.ceil(12000./SampleRateRef*FFTSize)) # only for calculate power in GPL
ImgFreqDim = IndFreqHigh - IndFreqLow


def DetectViaPowerLaw(DaySound, SelTabPath):
    DaySound = DaySound.replace('\\','/')
    #SoundList = sorted(glob.glob(os.path.join(DaySound+'/', '*.aif')))
    SoundList = sorted(glob.glob(os.path.join(DaySound+'/', '*.flac')))
#    SoundListTemp = glob.glob(os.path.join(DaySound+'/', '*.aif'))
#    if len(SoundListTemp) ==0:
#        SoundListTemp = glob.glob(os.path.join(DaySound+'/', '*.wav'))
#        if len(SoundListTemp) ==0:
#            return "Therea are no supported sound formats in .wav or .aif."
#    SoundList = sorted(SoundListTemp)     
    
    SelTabPath = SelTabPath.replace('\\','/')
    DayFile = os.path.join(SelTabPath+'/',os.path.basename(DaySound)+'.txt')
    
    ImageVecList = []    
    #if not os.path.exists(DayFile):
    if True:
        f = open(DayFile,'w')
        f.write('Selection\tView\tChannel\tBegin Time (s)\tEnd Time (s)\tLow Freq (Hz)\tHigh Freq (Hz)\tScore\tDuration\tBegin Path\tFile Offset (s)')
        f.write('\n')
        EventId=0
        
        #ffCount = 0
        for ff in SoundList:
        #for ff in SoundList[28:48:5]:
        #for ff in SoundList[10]:
            ff2 = os.path.splitext(os.path.basename(ff))[0]
            ##print ff2
            #logging.info(ff2)
            #logging.warning(ff2)
            
            #if(ffCount == 29 or ffCount == 30):
            #    logging.info('Stop!!')
            
            # time stamp
            TimeCurr = GetTimeStamp(ff2)
            ##print TimeCurr
            logging.info(str(TimeCurr.hour)+':'+str(TimeCurr.minute)+':'+str(TimeCurr.second))
            # read sound file
            Samples0, SampleRate0 = SoundRead(ff)
            # resampling 
            if (SampleRate0 != SampleRateRef):
                Samples0 = signal.resample(Samples0, float(len(Samples0))/SampleRate0*SampleRateRef)
            
            # make single-channel sound multi-dimensional array
            if(Samples0.ndim == 1):
                #Samples0 = np.array([Samples0]).T
                Samples0 = Samples0[:,None]
            
            for cc in range(Samples0.shape[1]):            
            #for cc in range(2, 3): # channel number
                ##print "Channel "+str(cc)
                
                ss_last = int(np.floor(Samples0.shape[0]*1.0/SampleRateRef/SegLen))
                #for ss in range(ss_last+1): # fixed the problem on SampleRate. Use SampleRate0 mistakenly at first.
                for ss in range(ss_last): 
                #for ss in range(2):
                    #if ss == ss_last-1:
                    #    Samples = Samples0[ss*SegLen*SampleRateRef:,cc]
                    #else:    
                    #    Samples = Samples0[ss*SegLen*SampleRateRef:(ss+1)*SegLen*SampleRateRef,cc]
                    Samples = Samples0[ss*SegLen*SampleRateRef:(ss+1)*SegLen*SampleRateRef,cc]
                    Samples = Samples - Samples.mean()
                    
                    # STFT
                    SftfMat = np.abs(librosa.stft(Samples, n_fft=FFTSize, hop_length=HopSize))
                    PowerLawStft = PowerLawMatCal(SftfMat, Nu1, Nu2, Gamma) # over time, freq
                    ##TDetecFunc = PowerLawStft.sum(axis=0) # summation over all frequency!?
                    TDetecFunc = PowerLawStft[IndFreqLow:IndFreqHigh,:].sum(axis=0) # summation over all frequency!?
                    TDetecRange = (TDetecFunc > TDetecThre).astype(int)
                    
                    TDetecRangeDiff = np.diff(TDetecRange)
                    IndDiff, ValDiff = FindIndexNonZero(TDetecRangeDiff)
                    
                    EventList = []
                    if ValDiff: # test if there's any detection result
                        if(ValDiff[0] == -1):
                            ValDiff = [1] + ValDiff
                            IndDiff = [0] + IndDiff
                        if(ValDiff[-1] == 1):
                            ValDiff.append(-1)
                            IndDiff.append(len(TDetecRangeDiff))
                        # Feature extraction based on detection results
                        for tt in range(0, len(ValDiff), 2):
                            TimeE1 = IndDiff[tt]
                            TimeE2 = (IndDiff[tt+1]+1)
                            TimeE_Cen = int(np.floor((IndDiff[tt]+(IndDiff[tt+1]+1))/2.))                            
                            
                            #if (TDetecThreLen <= (TimeE2-TimeE1)*EventTimeReso) and (TimeE_Cen-TimeE_DurHalf >= 0) and (TimeE_Cen+TimeE_DurHalf<TDetecRangeDiff.shape[0]):
                            if (TDetecThreLen <= (TimeE2-TimeE1)*EventTimeReso) and (TimeE_Cen-TimeE_DurHalf >= 0) and (TimeE_Cen+TimeE_DurHalf<TDetecRangeDiff.shape[0]):
                                #EventTVal = np.max(TDetecFunc[IndDiff[tt]:(IndDiff[tt+1]+1)]) # This is the score! It is the max value in the detection function.
                                EventTVal = np.percentile(TDetecFunc[IndDiff[tt]:(IndDiff[tt+1]+1)], 75) # This is the score! It is the max value in the detection function.
                                ImageVecCurr = PowerLawStft[IndFreqLow:IndFreqLow+ImgFreqDim,TimeE_Cen-TimeE_DurHalf:TimeE_Cen+TimeE_DurHalf]
                                EventList.append([TimeE1,TimeE2,EventTVal, ImageVecCurr.flatten()])
                        # write to selection table
                        for ee in range(len(EventList)):
                            # write to selection table
                            EventId += 1
                            Time1 = TimeCurr.hour*3600 + TimeCurr.minute*60.0 + TimeCurr.second + ss*SegLen*1.0 + EventList[ee][0]*EventTimeReso
                            Time2 = TimeCurr.hour*3600 + TimeCurr.minute*60.0 + TimeCurr.second + ss*SegLen*1.0 + EventList[ee][1]*EventTimeReso
                            TimeOffset = ss*SegLen*1.0 + EventList[ee][0]*EventTimeReso
                            f.write(str(EventId)+'\t'+'Spectrogram'+'\t'+str(cc+1)+'\t'+str.format("{0:=.4f}",Time1)+'\t'+str.format("{0:<.4f}",Time2)+'\t'+str.format("{0:=.1f}",50.0)+'\t'+str.format("{0:=.1f}",800.0)+'\t'+str.format("{0:<.5f}",EventList[ee][2])+'\t'+str.format("{0:<.5f}",Time2-Time1)+'\t'+ff+'\t'+str.format("{0:=.4f}",TimeOffset) )
                            #f.write('Selection\tView\tChannel\tBegin Time (s)\tEnd Time (s)\tLow Freq (Hz)\tHigh Freq (Hz)\tScore\tBegin Path\tFile Offset (s)')
                            f.write('\n')
                            ## collect image vectors
                            ImageVecList.append(EventList[ee][3])
            #ffCount += 1
        f.close()
        ## write to numpy array files / .npy
        np.save(os.path.join(SelTabPath+'/',os.path.basename(DaySound)+'.npy'), np.array(ImageVecList))
        del ImageVecList

        return os.path.basename(DaySound)+' was processed.'
    else:
        return os.path.basename(DaySound)+' already exists.'

if False:
    # Parameters
    Nu1 = 1.0
    Nu2 = 2.0
    # (2.5, 0.5) in GPL Matlab code
    Gamma = 1.0
    #SegLen = 60 # segment length for Power Law, 60 sec
    #SegLen = 90 # 90 sec; 1.5 min
    #SegLen = 180 # 3 min
    SegLen = 300 # 5 min
    SampleRateRef = 2000
    HopLength = 2**6 # step size
    #TDetecThre = 5e-4
    TDetecThre = 2e-4
    
    TDetecSmooth = 9 # median filter window for smoothing TDetec function; window length = 9*.032=
    TDetecThreLen = 0.40
    
    # FFT
    FFTSize = 512
    WinSize = FFTSize
    HopSize = 200 # 100 msec
    EventTimeReso = HopSize/float(SampleRateRef) # step size in sec
    
    TimeE_Dur = 2.0 # sec
    TimeE_DurHalf = int(np.floor(TimeE_Dur/2/EventTimeReso)) # int
    
    IndFreqLow = int(np.floor(50./SampleRateRef*FFTSize))
    IndFreqHigh = int(np.ceil(800./SampleRateRef*FFTSize))
    ImgFreqDim = 100

if __name__ == "__main__":
    
    #PROJ = 1 # CHAOZ-X
    PROJ = 3
    TEST_SNGLE_FILE = True
    #TEST_SNGLE_FILE = False
    SingleDep = True # True: single deployment; False: multiple deployment
    
    
    if PROJ == 1: # CHAOZ-X
        #WorkPath = r'/media/sf_2013_WHOI_Chukchi_71664/71664_WHOI01_20130820/71664_WHOI01_AIFF_PU197_2XB'
        #SelTabPath = r'/media/sf___ASA_Boston/__CHAOZX/2013'
        WorkPath = r'N:\projects\2013_WHOI_Chukchi_71664\71664_WHOI01_20130820\71664_WHOI01_AIFF_PU197_2XB'
        #SelTabPath = r'/media/sf___ASA_Boston/__CHAOZX/2013'
        SelTabPath = r'P:\users\yu_shiu_ys587\__Soundscape\__ASA_Boston\__CHAOZX\2013'
        
    elif PROJ == 2: # New Zealand
        WorkPath = r'/media/sf_S1068_NZ01_201612/S1068_NZ01_Swift_FLAC'
        SelTabPath = r'/media/sf___ASA_Boston/__NewZealand'
    elif PROJ == 3: # SSW
        #WorkPath = r'/media/sf_2016_BRP_SSW_S1067/S1067_SSW04_201703/S1067_SSW04_201703_FLAC'
        WorkPath = r'P:\projects\2016_BRP_SSW_S1067\S1067_SSW04_201703\S1067_SSW04_201703_FLAC'
        SelTabPath = r'P:\users\yu_shiu_ys587\__Soundscape\__ASA_Boston\__SSW\S1067_SSW04_201703'
        
    WorkPath = WorkPath.replace('\\','/')
    #DayList = sorted(glob.glob(os.path.join(WorkPath+'/','*'))) # << === problem!! We have 30 sites!    
    #DayList = sorted(glob.glob(os.path.join(WorkPath+'/'+r'S1067SSW04_S01_SWIFT28'+'/','*'))) 
    DayList2D = []
    for Site in sorted(glob.glob(os.path.join(WorkPath+'/','*'))):
        DayList2D.append(sorted(glob.glob(os.path.join(Site+'/','*'))))
    DayList = [OneDay for OneSite in DayList2D for OneDay in OneSite ]
    
    # Test selected days
    #DayList = DayList[20:210:30]
    SelTabPathList = [SelTabPath] * len(DayList) # make it a list for imap_unordered use            

    
#    DayList = []
#    SelTabPathList = []
#    for ww in range(len(WorkPath)):
#        WorkPathCurr = WorkPath[ww]
#        WorkPathCurr = WorkPathCurr.replace('\\','/')
#        DayListCurr = sorted(glob.glob(os.path.join(WorkPathCurr+'/','*')))
#        DayList = DayList + DayListCurr
#        SelTabPathList = SelTabPathList + [SelTabPath[ww]] * len(DayListCurr)

    if TEST_SNGLE_FILE == True:
        #TheDay = 11
        TheDay = 296 # S1067SSW04_S15_SWIFT08_20170401
        print DayList[TheDay]
        ReturnedStr = DetectViaPowerLaw(DayList[TheDay], SelTabPathList[TheDay])
        #for ii in range(7):
        #   print DayList[ii]
        #    DetectViaPowerLaw(DayList[ii], SelTabPathList[ii])
    else:
        MyPool = Pool(4) 
        try:
            for ReturnedStr in MyPool.imap_unordered(universal_worker, pool_args(DetectViaPowerLaw, DayList, SelTabPathList)):
            #for ReturnedStr in MyPool.imap_unordered(universal_worker, pool_args(DetectViaPowerLaw, DayList2, SelTabPathList)):
                print ReturnedStr
                logging.info(ReturnedStr)
            #print "Waiting 10 seconds"
            time.sleep(10)
        except KeyboardInterrupt:
            print "Caught KeyboardInterrupt, terminating workers"
            MyPool.terminate()
            MyPool.join()
        else:
            print "Work is finished. Quitting gracefully"
            MyPool.close()
            MyPool.join()
    
    
    
    
    
#    #SoundDir = r'/media/sf_2013_WHOI_Chukchi_71664/71664_WHOI01_20130820/71664_WHOI01_AIFF_PU197_2XB'
#    #71664_WHOI01_PU197_2XB_20130912
#    
#    if SingleDep: # Single Deployment
#        if PROJ == 1: # CHAOZ-X
#            #WorkPath = r'/media/sf_2013_WHOI_Chukchi_71664/71664_WHOI01_20130820/71664_WHOI01_AIFF_PU197_2XB'
#            #SelTabPath = r'/media/sf___ASA_Boston/__CHAOZX/2013'
#            WorkPath = r'N:\projects\2013_WHOI_Chukchi_71664\71664_WHOI01_20130820\71664_WHOI01_AIFF_PU197_2XB'
#            #SelTabPath = r'/media/sf___ASA_Boston/__CHAOZX/2013'
#            SelTabPath = r'P:\users\yu_shiu_ys587\__Soundscape\__ASA_Boston\__CHAOZX\2013'
#            
#        elif PROJ == 2: # New Zealand
#            WorkPath = r'/media/sf_S1068_NZ01_201612/S1068_NZ01_Swift_FLAC'
#            SelTabPath = r'/media/sf___ASA_Boston/__NewZealand'
#        elif PROJ == 3: # SSW
#            WorkPath = r'/media/sf_2016_BRP_SSW_S1067/S1067_SSW04_201703/S1067_SSW04_201703_FLAC'
#            SelTabPath = r'/media/sf___ASA_Boston/__SSW'
#            
#        WorkPath = WorkPath.replace('\\','/')
#        DayList = sorted(glob.glob(os.path.join(WorkPath+'/','*')))        
#        # Test selected days
#        #DayList = DayList[20:210:30]
#        SelTabPathList = [SelTabPath] * len(DayList) # make it a list for imap_unordered use            
#    else: # multiple Deployment
#        WorkPath = [r'/media/sf_2013_WHOI_Chukchi_71664/71664_WHOI01_20130820/71664_WHOI01_AIFF_PU197_2XB',
#                    r'/media/sf_2013_WHOI_Chukchi_71664/71664_WHOI01_20130820/71664_WHOI01_AIFF_PU197_2XB']
#
#        SelTabPath = [r'/media/sf___ASA_Boston/__CHAOZX/2013',
#                      r'/media/sf___ASA_Boston/__CHAOZX/2014']
#        
#        DayList = []
#        SelTabPathList = []
#        for ww in range(len(WorkPath)):
#            WorkPathCurr = WorkPath[ww]
#            WorkPathCurr = WorkPathCurr.replace('\\','/')
#            DayListCurr = sorted(glob.glob(os.path.join(WorkPathCurr+'/','*')))
#            DayList = DayList + DayListCurr
#            SelTabPathList = SelTabPathList + [SelTabPath[ww]] * len(DayListCurr)
#
#    if TEST_SNGLE_FILE == True:
#        #TheDay = 11
#        TheDay = 23
#        print DayList[TheDay]
#        ReturnedStr = DetectViaPowerLaw(DayList[TheDay], SelTabPathList[TheDay])
#        #for ii in range(7):
#        #   print DayList[ii]
#        #    DetectViaPowerLaw(DayList[ii], SelTabPathList[ii])
#    else:
#        MyPool = Pool(7) 
#        try:
#            for ReturnedStr in MyPool.imap_unordered(universal_worker, pool_args(DetectViaPowerLaw, DayList, SelTabPathList)):
#            #for ReturnedStr in MyPool.imap_unordered(universal_worker, pool_args(DetectViaPowerLaw, DayList2, SelTabPathList)):
#                print ReturnedStr
#                logging.info(ReturnedStr)
#            #print "Waiting 10 seconds"
#            time.sleep(10)
#        except KeyboardInterrupt:
#            print "Caught KeyboardInterrupt, terminating workers"
#            MyPool.terminate()
#            MyPool.join()
#        else:
#            print "Work is finished. Quitting gracefully"
#            MyPool.close()
#            MyPool.join()
            
