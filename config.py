# -*- coding: utf-8 -*-
"""
Created on Wed Nov 28 14:15:07 2018

@author: ys587
"""
import numpy as np
import math

class Config:
    #TEST_SNGLE_FILE = False # run in parallel for all days, all sites
    #SingleDep = True # True: single deployment; False: multiple deployment

    sound_path = r'/home/ys587/__Data/__Peru/__sound/'
    seltab_path = r'/home/ys587/__Data/__Peru/__seltab'

    deploy_name = 'T0009Peru01'
    site_list = ['SK1', 'SK2']
    
    FFTSize = 2048
    SampleRateRef = 48000
    
    
    Nu1 = 1.0
    Nu2 = 2.0
    Gamma = 1.0
    SegLen = 300 # 5 min = 300 sec
    # HopLength = 2**6 # step size
    #TDetecThre = 2e-4
    #TDetecThre = 1e-4
    #TDetecThre = 5e-6
    
    TDetecSmooth = 9 # median filter window for smoothing TDetec function; window length = 9*.032=
    #TDetecThreLen = 0.10 # minimum duration
    TDetecThreLen = 0.05 # minimum duration!!
    #TimeE_Dur = 1.0 # sec; duration for extracting features from spectrogram; 20171010
    TimeE_DurSec = 0.20
    #TimeE_Dur = TDetecThreLen/2. # incorrect since TDetecThreLen is the minimum duration. Too short!
    
    # FFT
    #FFTSize = 512
    #FFTSize = 256
    WinSize = FFTSize
    #HopSize = 200 # 100 msec
    #HopSize = 1600 # 50 msec
    #HopSize = 800 # 25 msec; 20171010
    #HopSize = 320 # 10 msec; 20171010
    HopSize = SampleRateRef*0.1
    
    EventTimeReso = HopSize/float(SampleRateRef) # step size in sec
    TimeE_DurHalf = int(np.floor(TimeE_DurSec/2/EventTimeReso)) # int
    TimeE_Dur = int(np.floor(TimeE_DurSec/EventTimeReso)) # int
    
    IndFreqLow = int(np.floor(500./SampleRateRef*FFTSize)) # used for both GPL power as well as the spectrogram image starting point
    IndFreqHigh = int(np.ceil(10500./SampleRateRef*FFTSize)) # only for calculate power in GPL
    ImgFreqDim = IndFreqHigh - IndFreqLow
    
    ThetaBinNum = 18 # bin number for HoG
    ThetaBinDeg = math.pi/ThetaBinNum
    NumCellTime =  2 # divided into 2 overlapped cells, i.e., 3 cells
    #NumCellTime =  1
    NumCellFreq =  1 # divided into 4 overlapped cells, i.e., 5 cells 
    
    FreqLow = float(SampleRateRef)*0.05
    FreqHigh = float(SampleRateRef)*0.25 # 8000 Hz          
    
    FreqHighMel = 10000.
    NumMFCC = 20
    
    N_read = SegLen*SampleRateRef
    N_previous = 0

    def __init__(sewlf):
        return None